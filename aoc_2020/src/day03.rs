pub fn run(input: &str) -> anyhow::Result<(String, String)> {
    let lines: Vec<Vec<char>> = input.lines().map(|f| f.chars().collect()).collect();
    let mut all_count = Vec::new();
    let size = [lines.len(), lines[0].len()];

    for dir in &[[1, 3], [1, 1], [1, 5], [1, 7], [2, 1]] {
        let map = lines.clone();
        let mut count = 0;

        let mut x = 0;
        let mut y = 0;
        loop {
            x += dir[0];
            y = (y + dir[1]) % size[1];

            if x >= size[0] {
                all_count.push(count);
                break;
            }

            if map[x][y] == '#' {
                count += 1;
            }
        }
    }

    Ok((
        all_count[0].to_string(),
        all_count.iter().product::<i64>().to_string(),
    ))
}

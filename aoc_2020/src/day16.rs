use std::{
    collections::{HashMap, HashSet},
    ops::Range,
    str::FromStr,
};

#[derive(Debug, Clone)]
struct PuzzleInput {
    pub fields: HashMap<String, Vec<Range<i32>>>,
    pub my_ticket: Vec<i32>,
    pub other_tickets: Vec<Vec<i32>>,
}

impl FromStr for PuzzleInput {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let s_ = s.replace("\r\n", "\n");
        let mut iter = s_.split("\n\n");
        let fields = iter
            .next()
            .unwrap()
            .split('\n')
            .map(|x| {
                let split_index = x.find(':').unwrap();
                let field_name = x.get(0..split_index).unwrap().to_string();
                let ranges: Vec<_> = x
                    .get(split_index + 2..)
                    .unwrap()
                    .replace(' ', "")
                    .split("or")
                    .map(|y| {
                        let mut iter = y.split('-');
                        (iter.next().unwrap().parse::<i32>().unwrap())
                            ..(iter.next().unwrap().parse::<i32>().unwrap() + 1)
                    })
                    .collect();

                (field_name, ranges)
            })
            .collect();

        let my_ticket = iter
            .next()
            .unwrap()
            .split('\n')
            .nth(1)
            .unwrap()
            .split(',')
            .map(|x| x.parse().unwrap())
            .collect();

        let other_tickets = iter
            .next()
            .unwrap()
            .split('\n')
            .skip(1)
            .filter(|x| !x.is_empty())
            .map(|x| x.split(',').map(|y| y.parse().unwrap()).collect())
            .collect();

        Ok(PuzzleInput {
            fields,
            my_ticket,
            other_tickets,
        })
    }
}

pub fn run(input: &str) -> anyhow::Result<(String, String)> {
    let input: PuzzleInput = input.parse().unwrap();

    let result1: i32 = {
        let fields: Vec<_> = input.fields.iter().flat_map(|x| x.1).collect();

        input
            .other_tickets
            .iter()
            .flat_map(|ticket| ticket.iter())
            .filter(|num| !fields.iter().any(|field| field.contains(num)))
            .sum()
    };

    let result2: i64 = {
        let numbers: HashMap<usize, Vec<i32>> = {
            let mut numbers = HashMap::new();
            input
                .other_tickets
                .iter()
                .filter(|ticket| is_valid_ticket(ticket, &input.fields))
                .flat_map(|x| x.iter().enumerate().collect::<Vec<(usize, _)>>())
                .for_each(|(x, y)| {
                    numbers.entry(x).or_insert(Vec::new());
                    numbers.get_mut(&x).unwrap().push(*y);
                });

            numbers
        };

        let mut unknown: HashMap<String, HashSet<usize>> = input
            .fields
            .iter()
            .map(|(name, rules)| {
                (
                    name.clone(),
                    numbers
                        .iter()
                        .filter(|(_, nums)| {
                            nums.iter().all(|x| rules.iter().any(|f| f.contains(x)))
                        })
                        .map(|(idx, _)| *idx)
                        .collect::<HashSet<usize>>(),
                )
            })
            .collect();

        let mut known = HashMap::<String, usize>::new();
        while !unknown.is_empty() {
            let (field, idx) = unknown
                .iter()
                .filter(|(_, indices)| indices.len() == 1)
                .map(|(f, i)| (f.clone(), *i.iter().next().unwrap()))
                .next()
                .unwrap();

            unknown.remove(&field);
            known.insert(field, idx);

            for indices in unknown.values_mut() {
                indices.remove(&idx);
            }
        }

        known
            .iter()
            .filter(|(field, _)| field.starts_with("departure "))
            .map(|(_, idx)| input.my_ticket[*idx] as i64)
            .product()
    };
    Ok((result1.to_string(), result2.to_string()))
}

fn is_valid_ticket(nums: &[i32], fields: &HashMap<String, Vec<Range<i32>>>) -> bool {
    nums.iter()
        .all(|num| fields.values().flatten().any(|field| field.contains(num)))
}

#[test]
fn test_1() {
    let input = "class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12";

    let result = run(input);
    assert!(result.is_ok());
    let result = result.unwrap();

    println!("Result: {}, {}", result.0, result.1);
    assert_eq!(result.0, "71");
}

#[test]
fn test_2() {
    let input = "class: 0-1 or 4-19
row: 0-5 or 8-19
seat: 0-13 or 16-19

your ticket:
11,12,13

nearby tickets:
3,9,18
15,1,5
5,14,9";

    let result = run(input);
    assert!(result.is_ok());
    let result = result.unwrap();

    println!("Result: {}, {}", result.0, result.1);
}

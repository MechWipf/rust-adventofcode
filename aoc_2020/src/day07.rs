use std::collections::HashMap;

type BagLookup = HashMap<String, Vec<(i32, String)>>;

// Is this some kind of read-only code? What have I done...
pub fn run(input: &str) -> anyhow::Result<(String, String)> {
    let input: &BagLookup = &input
        .lines()
        .map(|x| {
            let bags: Vec<String> = x
                .replace(", ", ",")
                .replace('.', "")
                .replace("bags", "bag")
                .split("contain")
                .map(|y| y.trim().to_string())
                .collect();

            (
                bags[0].to_string(),
                match bags[1].as_ref() {
                    "no other bag" => vec![(0, "no other bag".to_string())],
                    y => y
                        .split(',')
                        .map(|y| y.trim())
                        .map(|y| {
                            let num_pos = y.find(' ').unwrap();
                            (y[0..num_pos].parse().unwrap(), y[num_pos + 1..].to_string())
                        })
                        .collect(),
                },
            )
        })
        .collect();

    let mut cache: HashMap<String, bool> = HashMap::new();
    let result1 = input
        .iter()
        .filter(|x| contains_bag(input, &mut cache, "shiny gold bag", x.0))
        .count();

    let mut cache: HashMap<String, i64> = HashMap::new();
    let result2 = contains_bags(input, &mut cache, "shiny gold bag");

    /*{ Special part 3. List all bags and how many bags they have to contain.
        use itertools::Itertools;

        let special: String = input
            .iter()
            .map(|x| (x.0, contains_bags(input, &mut cache, &x.0)))
            .sorted_by(|a, b| Ord::cmp(&b.1, &a.1))
            .map(|x| format!("{}: {}", x.0, x.1))
            .join("\n");

        print!("{}", special);
    }*/

    Ok((result1.to_string(), result2.to_string()))
}

fn contains_bag(
    lookup: &BagLookup,
    cache: &mut HashMap<String, bool>,
    bag: &str,
    other_bag: &str,
) -> bool {
    if other_bag == "no other bag" {
        return false;
    }

    if !cache.contains_key(other_bag) {
        let bags = &lookup[other_bag];
        let item = bags
            .iter()
            .any(|x| x.1.contains(bag) || contains_bag(lookup, cache, bag, &x.1));
        cache.insert(other_bag.to_string(), item);
    }

    cache[other_bag]
}

fn contains_bags(lookup: &BagLookup, cache: &mut HashMap<String, i64>, bag: &str) -> i64 {
    if bag == "no other bag" {
        return 0;
    }

    if !cache.contains_key(bag) {
        let item = lookup[bag]
            .iter()
            .map(|x| x.0 as i64 + x.0 as i64 * contains_bags(lookup, cache, &x.1))
            .sum();

        cache.insert(bag.to_string(), item);
    }

    cache[bag]
}

use std::fmt::Display;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum SeatType {
    Unknown,
    Floor,
    Empty,
    Occupied,
}

impl From<char> for SeatType {
    fn from(c: char) -> Self {
        match c {
            '#' => SeatType::Occupied,
            '.' => SeatType::Floor,
            'L' => SeatType::Empty,
            _ => SeatType::Unknown,
        }
    }
}

impl Display for SeatType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        let seat = match self {
            SeatType::Unknown => "X",
            SeatType::Floor => ".",
            SeatType::Empty => "L",
            SeatType::Occupied => "#",
        };

        write!(f, "{}", seat)
    }
}

pub fn run(input: &str) -> anyhow::Result<(String, String)> {
    let input: &Vec<Vec<SeatType>> = &input
        .lines()
        .map(|x| x.chars().map(|c| c.into()).collect())
        .collect();

    let result1 = simulate(input, 4, false);

    let result2 = simulate(input, 5, true);

    Ok((result1.to_string(), result2.to_string()))
}

fn simulate(start_map: &[Vec<SeatType>], tollerance: i32, trace: bool) -> i32 {
    let mut map = start_map.to_vec();
    loop {
        let mut new_map = map.clone();

        for x in 0..new_map.len() {
            for y in 0..new_map[0].len() {
                let count = if trace {
                    count_adjacent_trace(&map, (x as i32, y as i32))
                } else {
                    count_adjacent(&map, (x as i32, y as i32))
                };

                if count == 0 && map[x][y] == SeatType::Empty {
                    new_map[x][y] = SeatType::Occupied
                } else if count >= tollerance && map[x][y] == SeatType::Occupied {
                    new_map[x][y] = SeatType::Empty
                }
            }
        }

        if vec_deep_eq(
            &map.iter().flatten().cloned().collect::<Vec<SeatType>>(),
            &new_map.iter().flatten().cloned().collect::<Vec<SeatType>>(),
        ) {
            return new_map
                .iter()
                .flatten()
                .filter(|x| **x == SeatType::Occupied)
                .count() as i32;
        }

        map = new_map.clone();
    }
}

fn count_adjacent(map: &[Vec<SeatType>], pos: (i32, i32)) -> i32 {
    let mut count = 0;
    for x in (pos.0 - 1)..(pos.0 + 2) {
        for y in (pos.1 - 1)..(pos.1 + 2) {
            if (x >= 0 && x < map.len() as i32)
                && (y >= 0 && y < map[0].len() as i32)
                && !(x == pos.0 && y == pos.1)
                && (map[x as usize][y as usize] == SeatType::Occupied)
            {
                count += 1;
            }
        }
    }

    count
}

fn count_adjacent_trace(map: &[Vec<SeatType>], pos: (i32, i32)) -> i32 {
    let mut count = 0;
    for x in -1..2 {
        for y in -1..2 {
            if !(x == 0 && y == 0) {
                let mut c_pos = pos;

                loop {
                    c_pos.0 += x;
                    c_pos.1 += y;

                    if (c_pos.0 >= 0 && c_pos.0 < map.len() as i32)
                        && (c_pos.1 >= 0 && c_pos.1 < map[0].len() as i32)
                        && !(c_pos.0 == pos.0 && c_pos.1 == pos.1)
                    {
                        if map[c_pos.0 as usize][c_pos.1 as usize] == SeatType::Occupied {
                            count += 1;
                            break;
                        } else if map[c_pos.0 as usize][c_pos.1 as usize] != SeatType::Floor {
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
        }
    }

    count
}

fn vec_deep_eq<T>(v1: &[T], v2: &[T]) -> bool
where
    T: PartialEq,
{
    if v1.len() != v2.len() {
        return false;
    }

    v1.iter().enumerate().all(|(i, val)| v2[i] == *val)
}

#[test]
fn count_adjacent_test() {
    let inp = "###\n###\n###\n";
    let map: &Vec<Vec<SeatType>> = &inp
        .lines()
        .map(|x| x.chars().map(|c| c.into()).collect())
        .collect();

    assert_eq!(8, count_adjacent(map, (1, 1)));
}

#[test]
fn count_adjacent_trace_test() {
    let inp = "###\n###\n###\n";
    let map: &Vec<Vec<SeatType>> = &inp
        .lines()
        .map(|x| x.chars().map(|c| c.into()).collect())
        .collect();

    assert_eq!(8, count_adjacent_trace(map, (1, 1)));

    let inp = "#.#.#\n.....\n#.L.#\n.....\n#.#.#\n";
    let map: &Vec<Vec<SeatType>> = &inp
        .lines()
        .map(|x| x.chars().map(|c| c.into()).collect())
        .collect();

    assert_eq!(8, count_adjacent_trace(map, (2, 2)));

    let inp = ".##.##.\n#.#.#.#\n##...##\n...L...\n##...##\n#.#.#.#\n.##.##.\n";
    let map: &Vec<Vec<SeatType>> = &inp
        .lines()
        .map(|x| x.chars().map(|c| c.into()).collect())
        .collect();

    assert_eq!(0, count_adjacent_trace(map, (3, 3)));
}

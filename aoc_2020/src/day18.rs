use lazy_static::lazy_static;
use regex::Regex;
use std::collections::VecDeque;

type Int = i128;

pub fn run(input: &str) -> anyhow::Result<(String, String)> {
    let input: &Vec<&str> = &input.lines().collect();

    let result1 = { input.iter().map(|line| eval(line, false)).sum::<Int>() };
    let result2 = { input.iter().map(|line| eval(line, true)).sum::<Int>() };

    Ok((result1.to_string(), result2.to_string()))
}

fn eval(expr: &str, precedence: bool) -> Int {
    lazy_static! {
        static ref FIND_ADD_RX: Regex = Regex::new(r"(\d+ \+ \d+)").unwrap();
        static ref FIND_BRACKETS_RX: Regex = Regex::new(r"(\((?:[^\(]*?)\))").unwrap();
    }

    let mut expr = expr.to_string();

    while expr.find('(').is_some() {
        let m = Regex::find(&FIND_BRACKETS_RX, &expr).unwrap();
        let section = m.as_str();

        let solution = eval(section.get(1..).unwrap(), precedence);
        expr = expr.replace(section, &solution.to_string());
    }

    if precedence {
        while expr.find('+').is_some() {
            let m = Regex::find(&FIND_ADD_RX, &expr).unwrap();
            let section = m.as_str();

            let solution = eval(section, false);
            expr = expr.replace(section, &solution.to_string());
        }
    }

    let mut stack: VecDeque<&str> = expr
        .split(|c| c == ' ' || c == '(' || c == ')')
        .filter(|x| !x.is_empty())
        .collect();
    let mut solution = stack.pop_front().unwrap().parse::<Int>().unwrap();

    while !stack.is_empty() {
        let op = stack.pop_front().unwrap();
        let n2 = stack.pop_front().unwrap().parse::<Int>().unwrap();

        solution = match op {
            "*" => solution * n2,
            "+" => solution + n2,
            _ => unreachable!(),
        };
    }

    solution
}

#[test]
fn test_0() {
    let input = "1 + (2 * 3) + (4 * (5 + 6))";
    let result = run(input);
    assert!(result.is_ok());
    let result = result.unwrap();
    assert_eq!(result, ("51".to_string(), "51".to_string()));
}

#[test]
fn test_1() {
    let input = "2 * 3 + (4 * 5)";
    let result = run(input);
    assert!(result.is_ok());
    let result = result.unwrap();
    assert_eq!(result, ("26".to_string(), "46".to_string()));
}

#[test]
fn test_2() {
    let input = "5 + (8 * 3 + 9 + 3 * 4 * 3)";
    let result = run(input);
    assert!(result.is_ok());
    let result = result.unwrap();
    assert_eq!(result, ("437".to_string(), "1445".to_string()));
}

#[test]
fn test_3() {
    let input = "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))";
    let result = run(input);
    assert!(result.is_ok());
    let result = result.unwrap();
    assert_eq!(result, ("12240".to_string(), "669060".to_string()));
}

#[test]
fn test_4() {
    let input = "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2";
    let result = run(input);
    assert!(result.is_ok());
    let result = result.unwrap();
    assert_eq!(result, ("13632".to_string(), "23340".to_string()));
}

use anyhow::Result;

pub fn run(input: &str) -> Result<(String, String)> {
    let lines = input.lines();

    let mut count_1 = 0;
    let mut count_2 = 0;
    for line in lines {
        let pass_entry = {
            let mut p = line.split_ascii_whitespace();
            let mut p2 = p.next().unwrap().split('-');

            PassEntry {
                from: p2.next().unwrap().parse().unwrap(),
                to: p2.next().unwrap().parse().unwrap(),
                char: p.next().unwrap().as_bytes()[0] as _,
                pass: p.next().unwrap().to_string(),
            }
        };

        if pass_entry.is_valid_part1() {
            count_1 += 1;
        };

        if pass_entry.is_valid_part2() {
            count_2 += 1;
        };
    }

    Ok((count_1.to_string(), count_2.to_string()))
}

#[derive(Debug, Clone)]
struct PassEntry {
    pub from: i32,
    pub to: i32,
    pub char: char,
    pub pass: String,
}

impl PassEntry {
    fn is_valid_part1(&self) -> bool {
        let c = self
            .pass
            .bytes()
            .filter(|&b| b as char == self.char)
            .count();

        c >= self.from as _ && c <= self.to as _
    }

    fn is_valid_part2(&self) -> bool {
        let pos1 = {
            let i = (self.from - 1) as usize;
            self.from > 0 && self.pass.get(i..i + 1).unwrap().eq(&self.char.to_string())
        };

        let pos2 = {
            let i = (self.to - 1) as usize;
            self.to > 0 && self.pass.get(i..i + 1).unwrap().eq(&self.char.to_string())
        };

        (pos1 || pos2) && !(pos1 && pos2)
    }
}

pub fn run(input: &str) -> anyhow::Result<(String, String)> {
    let lines = input
        .lines()
        .map(|f| f.parse::<i32>().expect("invalid value"))
        .collect::<Vec<_>>();

    let mut result1 = 0;
    'outer1: for x in &lines {
        for y in &lines {
            if x + y == 2020 {
                result1 = x * y;
                break 'outer1;
            }
        }
    }

    let mut result2 = 0;
    'outer2: for x in &lines {
        for y in &lines {
            for z in &lines {
                if x + y + z == 2020 {
                    result2 = x * y * z;
                    break 'outer2;
                }
            }
        }
    }

    Ok((result1.to_string(), result2.to_string()))
}

use std::collections::HashMap;

type Int = i64;

pub fn run(input: &str) -> anyhow::Result<(String, String)> {
    let input: Vec<Int> = input
        .lines()
        .next()
        .unwrap()
        .split(',')
        .map(|x| x.parse().unwrap())
        .collect();

    let result1 = play(&input, 2020);
    let result2 = play(&input, 30000000);
    Ok((result1.to_string(), result2.to_string()))
}

fn play(starting_numbers: &[Int], rounds: Int) -> Int {
    let mut numbers: HashMap<Int, Int> = HashMap::new();
    let mut turn = 1;
    let mut last = 0;

    for i in starting_numbers {
        last = *i;
        numbers.insert(last, turn);

        turn += 1;
    }

    while turn <= rounds {
        let next = if numbers.contains_key(&last) {
            turn - 1 - numbers[&last]
        } else {
            0
        };

        numbers.insert(last, turn - 1);
        last = next;

        turn += 1;
    }

    last
}

#[test]
fn test_1() {
    let result = run("0,3,6");

    assert!(result.is_ok());
    assert_eq!(result.unwrap().0, "436");
}

use std::collections::HashMap;

type Int = i64;

#[derive(Debug, Clone)]
enum Rule {
    Single(Vec<Int>),
    Double(Vec<Int>, Vec<Int>),
    Char(char),
}

impl std::str::FromStr for Rule {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        if s.find('|').is_some() {
            let iter = &mut s.split('|').map(|f| f.trim());
            Ok(Rule::Double(
                iter.next()
                    .unwrap()
                    .split_ascii_whitespace()
                    .map(|f| f.parse().unwrap())
                    .collect(),
                iter.next()
                    .unwrap()
                    .split_ascii_whitespace()
                    .map(|f| f.parse().unwrap())
                    .collect(),
            ))
        } else if s.find('"').is_some() {
            let s = s.replace('"', "");
            let s = s.trim();
            Ok(Rule::Char(s.chars().next().unwrap()))
        } else {
            let iter = &mut s.split('|').map(|f| f.trim());
            Ok(Rule::Single(
                iter.next()
                    .unwrap()
                    .split_ascii_whitespace()
                    .map(|f| f.parse().unwrap())
                    .collect(),
            ))
        }
    }
}

pub fn run(input: &str) -> anyhow::Result<(String, String)> {
    let input = input.replace("\r\n", "\n");
    let input = &mut input.split("\n\n");
    let input: HashMap<Int, Rule> = input
        .next()
        .unwrap()
        .lines()
        .map(|line| {
            let iter = &mut line.split(':').map(|f| f.trim());
            (
                iter.next().unwrap().parse::<Int>().unwrap(),
                iter.next().unwrap(),
            )
        })
        .map(|(key, line)| (key, line.parse::<Rule>().unwrap()))
        .collect();

    println!("{:?}", input);

    let result1 = 0;
    let result2 = 0;
    Ok((result1.to_string(), result2.to_string()))
}

// fn possible_strings(rules: &HashMap<Int, Rule>, start: &Int) -> impl Iterator<Item = String> {
//     let rule = &rules[start];
//     match rule {
//         // Rule::Single(arr) => arr
//         // .iter()
//         // .map(|x| possible_strings(rules, x).fold(String::new(), |acc, x| acc + &x)),
//         // Rule::Double(arr1, arr2) => vec![arr1
//         //     .iter()
//         //     .map(|x| possible_strings(rules, x).fold(String::new(), |acc, x| acc + &x))]
//         // .iter(),
//         // Rule::Char(c) => vec![c.to_string()].into(),
//         _ => unreachable!(),
//     }
// }

#[test]
fn test_0() {
    let input = "0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: \"a\"
5: \"b\"

ababbb
bababa
abbbab
aaabbb
aaaabbb";

    let result = run(input).unwrap();
    assert_eq!(result.0, "2");
    assert_eq!(result.1, "0");
}

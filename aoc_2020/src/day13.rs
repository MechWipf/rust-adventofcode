type Int = i64;

pub fn run(input: &str) -> anyhow::Result<(String, String)> {
    let mut input = input.lines();
    let timestamp: Int = input.next().unwrap().parse().unwrap();
    let input: Vec<Int> = input
        .next()
        .unwrap()
        .split(',')
        .map(|y| if y != "x" { y.parse().unwrap() } else { 0 })
        .collect();

    let result1 = {
        let input: Vec<Int> = input.iter().filter(|x| **x != 0).cloned().collect();
        let mut ret = 0;
        for start in timestamp.. {
            if let Some(bus) = get_departing_bus(&input, start) {
                ret = (start - timestamp) * bus;
                break;
            }
        }

        ret
    };

    let result2 = {
        let input: Vec<(_, _)> = input
            .iter()
            .cloned()
            .enumerate()
            .filter(|(_, x)| *x != 0)
            .collect();

        let mut delta = 1;
        let mut t = 0;

        for (offset, dt) in &input {
            loop {
                if (t + (*offset as Int)) % dt == 0 {
                    break;
                }

                t += delta;
            }

            delta = lcm(delta, *dt);
        }

        t
    };
    Ok((result1.to_string(), result2.to_string()))
}

fn get_departing_bus(bus_list: &[Int], time: Int) -> Option<Int> {
    for bus in bus_list {
        if time % bus == 0 {
            return Some(*bus);
        }
    }

    None
}

fn lcm(first: Int, second: Int) -> Int {
    first * second / gcd(first, second)
}

fn gcd(first: Int, second: Int) -> Int {
    let mut max = first;
    let mut min = second;
    if min > max {
        std::mem::swap(&mut max, &mut min);
    }

    loop {
        let res = max % min;
        if res == 0 {
            return min;
        }

        max = min;
        min = res;
    }
}

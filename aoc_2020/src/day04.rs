use anyhow::Result;

pub fn run(input: &str) -> Result<(String, String)> {
    let lines: Vec<String> = input.split("\n\n").map(|f| f.replace('\n', " ")).collect();

    let stuff = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];
    let result1 = lines
        .iter()
        .filter(|f| stuff.iter().all(|&s| f.contains(s)));

    let result2 = result1
        .clone()
        .filter(|f| {
            f.split(' ')
                .filter(|f| !f.is_empty())
                .all(|s| validate(s.trim()))
        })
        .count();

    Ok((result1.count().to_string(), result2.to_string()))
}

fn validate(data: &str) -> bool {
    let (label, data) = {
        let mut p = data.split(':');
        (p.next().unwrap(), p.next().unwrap())
    };

    match label {
        "byr" => {
            if let Ok(n) = data.parse::<i32>() {
                (1920..=2002).contains(&n)
            } else {
                false
            }
        }
        "iyr" => {
            if let Ok(n) = data.parse::<i32>() {
                (2010..=2020).contains(&n)
            } else {
                false
            }
        }
        "eyr" => {
            if let Ok(n) = data.parse::<i32>() {
                (2020..=2030).contains(&n)
            } else {
                false
            }
        }
        "hgt" => {
            if data.ends_with("in") {
                if let Ok(n) = data.replace("in", "").parse::<i32>() {
                    (59..=76).contains(&n)
                } else {
                    false
                }
            } else if data.ends_with("cm") {
                if let Ok(n) = data.replace("cm", "").parse::<i32>() {
                    (150..=193).contains(&n)
                } else {
                    false
                }
            } else {
                false
            }
        }
        "hcl" => {
            const ALLOWED_CHARS: &[char; 16] = &[
                '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'a', 'b', 'c', 'd', 'e', 'f',
            ];

            if data.starts_with('#') && data.len() == 7 {
                data.chars().skip(1).all(|f| ALLOWED_CHARS.contains(&f))
            } else {
                false
            }
        }
        "ecl" => {
            const ALLOWED_COLOR: &[&str; 7] = &["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];
            ALLOWED_COLOR.contains(&data)
        }
        "pid" => {
            if data.len() == 9 {
                data.parse::<i32>().is_ok()
            } else {
                false
            }
        }
        "cid" => true,
        _ => false,
    }
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
enum Direction {
    North = 0,
    South = 90,
    East = 180,
    West = 270,
}

impl From<i32> for Direction {
    fn from(i: i32) -> Self {
        let i = if i < 0 {
            360 - (i % 360).abs()
        } else {
            i % 360
        };

        match i {
            0 => Direction::North,
            90 => Direction::South,
            180 => Direction::East,
            270 => Direction::West,
            x => panic!("Invalid number {}", x),
        }
    }
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
enum Command {
    Direction(Direction),
    Left,
    Right,
    Forward,
}

impl From<char> for Command {
    fn from(c: char) -> Self {
        match c {
            'N' => Command::Direction(Direction::North),
            'S' => Command::Direction(Direction::South),
            'E' => Command::Direction(Direction::East),
            'W' => Command::Direction(Direction::West),
            'L' => Command::Left,
            'R' => Command::Right,
            'F' => Command::Forward,
            _ => panic!("Invalid character"),
        }
    }
}

#[derive(Debug, Clone)]
struct Ship {
    pub facing: Direction,
    pub pos: (i64, i64),
}

impl Ship {
    pub fn new() -> Self {
        Self {
            facing: Direction::East,
            pos: (0, 0),
        }
    }

    pub fn run_commands(&mut self, commands: &[(Command, i64)]) {
        for cmd in commands {
            match cmd.0 {
                Command::Direction(x) => self.move_direction(x, cmd.1),
                Command::Forward => self.move_forward(cmd.1),
                Command::Left => self.rotate(Command::Left, cmd.1),
                Command::Right => self.rotate(Command::Right, cmd.1),
            }
        }
    }

    fn move_direction(&mut self, dir: Direction, amount: i64) {
        match dir {
            Direction::North => self.pos.1 -= amount,
            Direction::East => self.pos.0 += amount,
            Direction::South => self.pos.1 += amount,
            Direction::West => self.pos.0 -= amount,
        }
    }

    fn move_forward(&mut self, amount: i64) {
        self.move_direction(self.facing, amount)
    }

    fn rotate(&mut self, dir: Command, amount: i64) {
        let facing = match dir {
            Command::Left => self.facing as i64 + amount,
            Command::Right => self.facing as i64 - amount,
            _ => 0,
        };

        self.facing = (facing as i32).into();
    }
}

struct Navigation {
    pub ship: Ship,
    pub waypoint: (i64, i64),
}

impl Navigation {
    pub fn new() -> Self {
        Self {
            ship: Ship::new(),
            waypoint: (10, -1),
        }
    }

    pub fn run_commands(&mut self, commands: &[(Command, i64)]) {
        //println!("{:?} {:?}", self.waypoint, self.ship);

        for cmd in commands {
            match cmd.0 {
                Command::Direction(x) => self.move_direction(x, cmd.1),
                Command::Forward => self.advance_ship(cmd.1),
                dir if dir == Command::Left || dir == Command::Right => self.rotate(dir, cmd.1),
                _ => unreachable!(),
            }

            //println!("{:?} {:?}", self.waypoint, self.ship);
        }
    }

    fn move_direction(&mut self, dir: Direction, amount: i64) {
        match dir {
            Direction::North => self.waypoint.1 -= amount,
            Direction::East => self.waypoint.0 += amount,
            Direction::South => self.waypoint.1 += amount,
            Direction::West => self.waypoint.0 -= amount,
        }
    }

    fn rotate(&mut self, dir: Command, amount: i64) {
        let amount = if dir == Command::Left {
            360 - amount
        } else {
            amount
        };

        match amount {
            0 => {}
            90 => self.waypoint = (-self.waypoint.1, self.waypoint.0),
            180 => self.waypoint = (-self.waypoint.0, -self.waypoint.1),
            270 => self.waypoint = (self.waypoint.1, -self.waypoint.0),
            _ => unreachable!(),
        }
    }

    fn advance_ship(&mut self, amount: i64) {
        self.ship.pos.0 += self.waypoint.0 * amount;
        self.ship.pos.1 += self.waypoint.1 * amount;
    }
}

pub fn run(input: &str) -> anyhow::Result<(String, String)> {
    let input: &Vec<(Command, i64)> = &input
        .lines()
        .map(|x| {
            (
                x.chars().next().unwrap().into(),
                x.get(1..).unwrap().parse().unwrap(),
            )
        })
        .collect();

    let result1 = {
        let mut ship = Ship::new();
        ship.run_commands(&input.clone());

        ship.pos.0 + ship.pos.1
    };

    let result2 = {
        let mut nav = Navigation::new();
        nav.run_commands(&input.clone());

        nav.ship.pos.0.abs() + nav.ship.pos.1.abs()
    };

    Ok((result1.to_string(), result2.to_string()))
}

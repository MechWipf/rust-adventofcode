use std::collections::HashSet;

pub fn run(input: &str) -> anyhow::Result<(String, String)> {
    let input: &Vec<Instruction> = &input.lines().map(Instruction::new_from_string).collect();

    let result1 = run_bootloader(input.as_slice()).1;

    let mut ops_to_change = input.iter().filter(|i| i.op_code != "acc").enumerate();
    let result2 = loop {
        match ops_to_change.next() {
            Some(ops) => {
                let mut memory: Vec<Instruction> = input.to_vec();

                if memory[ops.0].op_code == "jmp" {
                    memory[ops.0].op_code = "nop".to_string();
                } else {
                    memory[ops.0].op_code = "jmp".to_string();
                }

                let ret = run_bootloader(memory.as_slice());
                if !ret.0 {
                    break ret.1;
                }
            }
            _ => {
                break -1;
            }
        }
    };
    Ok((result1.to_string(), result2.to_string()))
}

#[derive(Debug, Clone)]
struct Instruction {
    pub op_code: String,
    pub param_1: i32,
}

impl Instruction {
    pub fn new_from_string(data: &str) -> Self {
        let spl: Vec<&str> = data.split(' ').collect();
        Instruction {
            op_code: spl[0].to_string(),
            param_1: spl[1].to_string().parse().unwrap(),
        }
    }
}

fn run_bootloader(memory: &[Instruction]) -> (bool, i32) {
    let mut loop_check: HashSet<i32> = HashSet::new();
    let mut pointer: i32 = 0;
    let mut acc = 0;

    loop {
        if pointer < 0 {
            pointer = 0;
        }
        let cell = &memory[pointer as usize];

        match cell.op_code.as_ref() {
            "acc" => {
                acc += cell.param_1;
                pointer += 1;
            }
            "jmp" => {
                pointer += cell.param_1;
            }
            "nop" => {
                pointer += 1;
            }
            _ => {}
        }

        if pointer >= memory.len() as i32 {
            break (false, acc);
        }

        if !loop_check.insert(pointer) {
            break (true, acc);
        }
    }
}

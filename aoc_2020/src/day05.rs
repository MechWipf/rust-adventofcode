use std::collections::HashSet;

pub fn run(input: &str) -> anyhow::Result<(String, String)> {
    let lines = input
        .lines()
        .map(|f| (f.get(..7).unwrap(), f.get(7..).unwrap()))
        .collect::<Vec<(&str, &str)>>();

    let lines1 = lines.iter().cloned().map(|line| {
        let mut row = 0..127;
        for row_search in line.0.chars() {
            let count = row.clone().count() + 1;
            if row_search == 'F' {
                row.end -= count / 2;
            } else {
                row.start += count / 2;
            }
        }

        let mut col = 0..7;
        for col_search in line.1.chars() {
            let count = col.clone().count() + 1;
            if col_search == 'L' {
                col.end -= count / 2;
            } else {
                col.start += count / 2;
            }
        }

        row.start * 8 + col.start
    });
    let result1 = lines1.clone().max().unwrap();

    let lines2 = lines1.clone().collect::<HashSet<usize>>();
    let result2 = (0..127)
        .flat_map(|r| (0..7).map(move |c| r * 8 + c))
        .filter(|id| !lines2.contains(id))
        .collect::<Vec<usize>>();
    let result2 = result2
        .iter()
        .cloned()
        .filter(|&id| id > 0 && lines2.contains(&(id + 1)) && lines2.contains(&(id - 1)))
        .collect::<Vec<usize>>();

    Ok((result1.to_string(), result2.first().unwrap().to_string()))
}

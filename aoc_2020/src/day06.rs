use itertools::Itertools;
use std::collections::HashSet;

pub fn run(input: &str) -> anyhow::Result<(String, String)> {
    let input = input.split("\n\n").map(|data| {
        data.replace('\n', " ")
            .replace("  ", " ")
            .trim_end()
            .split_ascii_whitespace()
            .map(|d| d.to_string())
            .collect::<Vec<String>>()
    });

    let result1: i64 = input
        .clone()
        .map(|x| x.concat().chars().collect::<HashSet<char>>())
        .map(|x| x.len() as i64)
        .sum();

    let result2: i64 = input
        .into_iter()
        .map(|x| {
            collect_all_answers(
                x.iter()
                    .map(|y| y.chars().collect::<HashSet<char>>())
                    .collect(),
            )
        })
        .sum();

    Ok((result1.to_string(), result2.to_string()))
}

fn collect_all_answers(it: Vec<HashSet<char>>) -> i64 {
    let people = it.len() as i64;
    it.iter()
        .flat_map(|x| x.iter().map(|y| (y, 1)).collect::<Vec<_>>())
        .into_group_map()
        .into_iter()
        .filter(|x| x.1.len() as i64 == people)
        .count() as i64
}

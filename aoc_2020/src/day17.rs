use std::{collections::HashSet, fmt, ops::Range};

#[derive(Debug, Clone)]
struct ConwayMap2(HashSet<(i32, i32)>);
#[derive(Debug, Clone)]
struct ConwayMap3(HashSet<(i32, i32, i32)>);
#[derive(Debug, Clone)]
struct ConwayMap4(HashSet<(i32, i32, i32, i32)>);

impl fmt::Display for ConwayMap3 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let (x_range, y_range, z_range) = self.size();

        for z in z_range.clone() {
            writeln!(f, "z={}", z)?;

            for y in y_range.clone() {
                for x in x_range.clone() {
                    let cell = (x, y, z);
                    if self.0.contains(&cell) {
                        write!(f, "#")?;
                    } else {
                        write!(f, ".")?;
                    }
                }

                writeln!(f)?;
            }
        }

        writeln!(f)
    }
}

impl ConwayMap3 {
    pub fn calc(&self) -> Self {
        let (x_range, y_range, z_range) = self.size();

        let mut map = Self(HashSet::new());
        for x in x_range.clone() {
            for y in y_range.clone() {
                for z in z_range.clone() {
                    let cell = (x, y, z);
                    if self.calc_single_state(cell) {
                        map.0.insert(cell);
                    }
                }
            }
        }

        map
    }

    fn size(&self) -> (Range<i32>, Range<i32>, Range<i32>) {
        if self.0.is_empty() {
            return (0..1, 0..1, 0..1);
        }

        let x_range = self.0.iter().map(|(x, _, _)| x).min().unwrap() - 1
            ..self.0.iter().map(|(x, _, _)| x).max().unwrap() + 2;
        let y_range = self.0.iter().map(|(_, y, _)| y).min().unwrap() - 1
            ..self.0.iter().map(|(_, y, _)| y).max().unwrap() + 2;
        let z_range = self.0.iter().map(|(_, _, z)| z).min().unwrap() - 1
            ..self.0.iter().map(|(_, _, z)| z).max().unwrap() + 2;

        (x_range, y_range, z_range)
    }

    fn calc_single_state(&self, cell: (i32, i32, i32)) -> bool {
        let mut count = 0;

        for x in cell.0 - 1..cell.0 + 2 {
            for y in cell.1 - 1..cell.1 + 2 {
                for z in cell.2 - 1..cell.2 + 2 {
                    let key = (x, y, z);
                    if key != cell && self.0.contains(&key) {
                        count += 1;
                    }
                }
            }
        }

        count == 3 || self.0.contains(&cell) && count == 2
    }
}

impl ConwayMap4 {
    pub fn calc(&self) -> Self {
        let (x_range, y_range, z_range, w_range) = self.size();

        let mut map = Self(HashSet::new());
        for x in x_range.clone() {
            for y in y_range.clone() {
                for z in z_range.clone() {
                    for w in w_range.clone() {
                        let cell = (x, y, z, w);
                        if self.calc_single_state(cell) {
                            map.0.insert(cell);
                        }
                    }
                }
            }
        }

        map
    }

    fn size(&self) -> (Range<i32>, Range<i32>, Range<i32>, Range<i32>) {
        if self.0.is_empty() {
            return (0..1, 0..1, 0..1, 0..1);
        }

        let x_range = self.0.iter().map(|(x, _, _, _)| x).min().unwrap() - 1
            ..self.0.iter().map(|(x, _, _, _)| x).max().unwrap() + 2;
        let y_range = self.0.iter().map(|(_, y, _, _)| y).min().unwrap() - 1
            ..self.0.iter().map(|(_, y, _, _)| y).max().unwrap() + 2;
        let z_range = self.0.iter().map(|(_, _, z, _)| z).min().unwrap() - 1
            ..self.0.iter().map(|(_, _, z, _)| z).max().unwrap() + 2;
        let w_range = self.0.iter().map(|(_, _, _, w)| w).min().unwrap() - 1
            ..self.0.iter().map(|(_, _, _, w)| w).max().unwrap() + 2;

        (x_range, y_range, z_range, w_range)
    }

    fn calc_single_state(&self, cell: (i32, i32, i32, i32)) -> bool {
        let mut count = 0;

        for x in cell.0 - 1..cell.0 + 2 {
            for y in cell.1 - 1..cell.1 + 2 {
                for z in cell.2 - 1..cell.2 + 2 {
                    for w in cell.3 - 1..cell.3 + 2 {
                        let key = (x, y, z, w);
                        if key != cell && self.0.contains(&key) {
                            count += 1;
                        }
                    }
                }
            }
        }

        count == 3 || self.0.contains(&cell) && count == 2
    }
}

pub fn run(input: &str) -> anyhow::Result<(String, String)> {
    let input: ConwayMap2 = ConwayMap2(
        input
            .lines()
            .enumerate()
            .flat_map(|(x, line)| {
                line.char_indices()
                    .filter(|(_, ch)| *ch == '#')
                    .map(move |(y, _)| (x as i32, y as i32))
            })
            .collect(),
    );

    let result1 = {
        let mut map = ConwayMap3(input.0.iter().map(|(x, y)| (*x, *y, 0)).collect());

        for _ in 0..6 {
            map = map.calc();
        }

        map.0.len()
    };

    let result2 = {
        let mut map = ConwayMap4(input.0.iter().map(|(x, y)| (*x, *y, 0, 0)).collect());

        for _ in 0..6 {
            map = map.calc();
        }

        map.0.len()
    };

    Ok((result1.to_string(), result2.to_string()))
}

#[test]
fn test_1() {
    let input = ".#.
..#
###
";

    println!("{:?}", (0..3).collect::<Vec<i32>>());

    let result = run(input);
    assert!(result.is_ok());
    let result = result.unwrap();

    assert_eq!(result.0, "112");
    assert_eq!(result.1, "848");
}

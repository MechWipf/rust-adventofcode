use itertools::Itertools;

pub fn run(input: &str) -> anyhow::Result<(String, String)> {
    let input: &Vec<i32> = &input.lines().map(|x| x.parse().unwrap()).collect();
    let end = [input.iter().max().unwrap() + 3];
    let input: &Vec<i32> = &[0].iter().chain(input).chain(end.iter()).cloned().collect();

    let result1 = {
        let mut diff = (0, 0);
        let mut last = -5;
        for x in input.iter().skip(1).sorted_by(Ord::cmp) {
            if x - last == 3 {
                diff.1 += 1;
            } else {
                diff.0 += 1;
            }
            last = *x;
        }

        diff.0 * diff.1
    };

    let result2 = {
        let chains: &Vec<i32> = &input.iter().cloned().sorted_by(Ord::cmp).collect();
        let max_joltage = *chains.iter().max().unwrap() as usize;
        let mut path: Vec<i64> = vec![0; max_joltage + 1];
        path[0] = 1;

        for adapter in chains {
            for diff in 1..4 {
                let next = *adapter + diff;
                if next as usize <= max_joltage && chains.contains(&next) {
                    path[next as usize] += path[*adapter as usize] as i64;
                }
            }
        }

        path[max_joltage]
    };
    Ok((result1.to_string(), result2.to_string()))
}

use std::collections::{HashMap, VecDeque};

pub fn run(input: &str) -> anyhow::Result<(String, String)> {
    let input: Vec<Entry> = input
        .lines()
        .map(|f| {
            if f.starts_with("mem") {
                let start = f.find('[').unwrap();
                let end = f.find(']').unwrap();
                let num1: u64 = f.get(start + 1..end).unwrap().parse().unwrap();
                let start = f.find('=').unwrap();
                let num2: u64 = f.get((start + 2)..).unwrap().parse().unwrap();

                Entry::Mem {
                    pos: num1,
                    value: num2,
                }
            } else if f.starts_with("mask") {
                let start = f.find('=').unwrap();
                let mask = f.get(start + 2..).unwrap();

                Entry::Mask(mask.to_string())
            } else {
                Entry::Unknown
            }
        })
        .collect();

    let result1: u64 = {
        let mut memory: HashMap<u64, u64> = HashMap::new();
        let mut current_mask: &str = "";

        for entry in &input {
            match entry {
                Entry::Mask(x) => current_mask = x,
                Entry::Mem { pos, value } => {
                    memory.insert(*pos, add_mask(current_mask, *value));
                }
                _ => unreachable!(),
            }
        }

        memory.values().sum()
    };

    let result2: u64 = {
        let mut memory: HashMap<u64, u64> = HashMap::new();
        let mut current_mask: &str = "";

        for entry in &input {
            match entry {
                Entry::Mask(x) => current_mask = x,
                Entry::Mem { pos, value } => mask_and_permutate(current_mask, *pos)
                    .iter()
                    .for_each(|pos| {
                        memory.insert(*pos, *value);
                    }),
                _ => unreachable!(),
            }
        }

        memory.values().sum()
    };

    Ok((result1.to_string(), result2.to_string()))
}

fn add_mask(mask: &str, n: u64) -> u64 {
    let mut ret = n;

    for (i, c) in mask.chars().rev().enumerate() {
        match c {
            '0' => {
                let int = (1 << i) ^ u64::MAX;
                ret &= int;
            }
            '1' => {
                let int = 1 << i;
                ret |= int;
            }
            _ => {}
        }
    }

    ret
}

fn mask_and_permutate(mask: &str, n: u64) -> Vec<u64> {
    let mut result: Vec<u64> = Vec::new();
    let mut queue: VecDeque<String> = VecDeque::new();
    queue.push_back(
        format!("{:036b}", n)
            .char_indices()
            .map(|c| {
                let mask_char = mask.chars().nth(c.0).unwrap();
                match mask_char {
                    '1' => '1',
                    'X' => 'X',
                    _ => c.1,
                }
            })
            .collect::<String>(),
    );

    while !queue.is_empty() {
        let v = queue.pop_front().unwrap();

        if let Some(x) = v.find('X') {
            let mut v_ = v.clone();
            v_.replace_range(x..x + 1, "1");
            queue.push_back(v_);

            let mut v_ = v.clone();
            v_.replace_range(x..x + 1, "0");
            queue.push_back(v_);
        } else {
            result.push(u64::from_str_radix(&v, 2).unwrap());
        }
    }

    result
}

#[test]
fn test_1() {
    let input =
        "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X\nmem[8] = 11\nmem[7] = 101\nmem[8] = 0\n";
    let res = run(input);

    assert!(res.is_ok());
    assert_eq!(res.unwrap().0, "165");
}

#[test]
fn test_2() {
    let input =
        "mask = 000000000000000000000000000000X1001X\nmem[42] = 100\nmask = 00000000000000000000000000000000X0XX\nmem[26] = 1";
    let res = run(input);

    assert!(res.is_ok());
    assert_eq!(res.unwrap().1, "208");
}

#[derive(Debug, Clone)]
enum Entry {
    Unknown,
    Mask(String),
    Mem { pos: u64, value: u64 },
}

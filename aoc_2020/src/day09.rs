const PREAMBLE: usize = 25;

pub fn run(input: &str) -> anyhow::Result<(String, String)> {
    let input: &Vec<i64> = &input.lines().map(|x| x.parse().unwrap()).collect();

    let mut result1: i64 = 0;
    for x in input.iter().skip(PREAMBLE).enumerate() {
        if !is_sum_of(
            input
                .iter()
                .cloned()
                .skip(x.0)
                .take(PREAMBLE)
                .collect::<Vec<i64>>()
                .as_slice(),
            x.1,
        ) {
            result1 = *x.1;
            break;
        }
    }
    let mut result2: i64 = 0;

    'outer: for x in 0..input.len() {
        for y in 2..input.len() {
            let mut range = input.iter().skip(x).take(y).cloned().collect::<Vec<i64>>();
            if range.iter().sum::<i64>().eq(&result1) {
                range.sort_by(Ord::cmp);
                result2 = range.first().unwrap() + range.last().unwrap();
                break 'outer;
            }
        }
    }

    Ok((result1.to_string(), result2.to_string()))
}

fn is_sum_of(preamble: &[i64], num: &i64) -> bool {
    for first in preamble {
        let search: i64 = num - first;

        if preamble.contains(&search) && &search != first {
            return true;
        }
    }

    false
}

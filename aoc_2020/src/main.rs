use anyhow::{Context, Result};
use clap::{App, Arg};
use std::fs::File;
use std::io::prelude::*;

mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
mod day07;
mod day08;
mod day09;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
mod day17;
mod day18;
mod day19;
mod day20;
mod day21;
mod day22;
mod day23;
mod day24;
mod day25;

const ARG_DAY: &str = "day";
const ARG_INPUT: &str = "input";

fn main() -> Result<()> {
    let matches = App::new("Mechwipf's Advent Of Code 2020 Solutions")
        .version("1.0")
        .author("MechWipf <MechWipf@googlemail.com>")
        .about("Advent of Code... what else")
        .arg(
            Arg::with_name(ARG_DAY)
                .short("d")
                .long("day")
                .value_name("INTEGER")
                .help("Specify which day to run")
                .takes_value(true),
        )
        .arg(
            Arg::with_name(ARG_INPUT)
                .short("i")
                .long("input")
                .value_name("FILE")
                .help("Puzzel input to run on")
                .required(true),
        )
        .get_matches();

    let day = matches.value_of(ARG_DAY).context("no day specified")?;
    let input = matches
        .value_of(ARG_INPUT)
        .context("no file with puzzel input provided")?;

    let mut file = File::open(input).context("failed to open input file")?;
    let mut s = String::new();
    file.read_to_string(&mut s)
        .context("failed to read puzzle input from file")?;

    let (res1, res2) = match day {
        "1" => day01::run(&s)?,
        "2" => day02::run(&s)?,
        "3" => day03::run(&s)?,
        "4" => day04::run(&s)?,
        "5" => day05::run(&s)?,
        "6" => day06::run(&s)?,
        "7" => day07::run(&s)?,
        "8" => day08::run(&s)?,
        "9" => day09::run(&s)?,
        "10" => day10::run(&s)?,
        "11" => day11::run(&s)?,
        "12" => day12::run(&s)?,
        "13" => day13::run(&s)?,
        "14" => day14::run(&s)?,
        "15" => day15::run(&s)?,
        "16" => day16::run(&s)?,
        "17" => day17::run(&s)?,
        "18" => day18::run(&s)?,
        "19" => day19::run(&s)?,
        "20" => day20::run(&s)?,
        "21" => day21::run(&s)?,
        "22" => day22::run(&s)?,
        "23" => day23::run(&s)?,
        "24" => day24::run(&s)?,
        "25" => day25::run(&s)?,
        _ => ("None".to_string(), "None".to_string()),
    };

    println!("Solution Day {} Part 1: {}", day, res1);
    println!("Solution Day {} Part 2: {}", day, res2);

    Ok(())
}

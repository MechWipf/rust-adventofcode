use std::collections::VecDeque;

use crate::Solution;
use eyre::Result;
use itertools::Itertools;

pub struct Day {}
impl Day {
    pub fn new() -> Day {
        Day {}
    }
}

impl Solution for Day {
    fn run(&self, input_str: &str) -> Result<()> {
        println!(
            "Part 1: {}\nPart 2: {}",
            part1(input_str)?,
            part2(input_str)?
        );

        Ok(())
    }
}

fn part1(input: &str) -> Result<i32> {
    solve(input, 4usize)
}

fn part2(input: &str) -> Result<i32> {
    solve(input, 14usize)
}

fn solve(input: &str, length: usize) -> Result<i32> {
    let memory: &mut VecDeque<char> = &mut VecDeque::from(vec![]);

    for (index, ch) in input.chars().enumerate() {
        memory.push_back(ch);
        if memory.len() > length {
            memory.pop_front();
        }

        if memory.iter_mut().sorted().dedup().count() == length {
            return Ok((index + 1) as _);
        }
    }

    Ok(-1)
}

#[cfg(test)]
mod test {
    use super::{part1, part2};

    #[test]
    fn test_one() {
        let input = include_str!("../input/day-6/example.txt");
        let value = part1(input).unwrap();

        assert_eq!(value, 7);
    }

    #[test]
    fn test_two() {
        let input = include_str!("../input/day-6/example.txt");
        let value = part2(input).unwrap();

        assert_eq!(value, 19);
    }
}

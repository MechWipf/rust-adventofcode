use crate::Solution;
use eyre::Result;
use itertools::Itertools;
use nom::{
    bytes::complete::take, character::complete::line_ending, combinator::map_res, multi::many1,
    sequence::terminated, IResult,
};
use std::fmt::Display;

pub struct Day {}
impl Day {
    pub fn new() -> Day {
        Day {}
    }
}

impl Solution for Day {
    fn run(&self, input_str: &str) -> Result<()> {
        println!(
            "Part 1: {}
Part 2: {}",
            part1(input_str)?,
            part2(input_str)?
        );

        Ok(())
    }
}

#[derive(Debug, Clone)]
struct Map {
    pub width: usize,
    pub height: usize,
    pub trees: Vec<u8>,
}

impl Map {
    pub fn get(&self, x: usize, y: usize) -> Option<u8> {
        if x >= self.width || y >= self.height {
            None
        } else {
            let index = y * self.width + x;
            self.trees.get(index).copied()
        }
    }
}

impl Display for Map {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for y in 0..self.height {
            for x in 0..self.width {
                let cell = self.get(x, y);
                write!(f, "{}", cell.unwrap())?
            }
            writeln!(f)?
        }

        write!(f, "")
    }
}

struct MapWithHighlight<'a> {
    map: &'a Map,
    hightlights: &'a [(i32, i32)],
}

impl Display for MapWithHighlight<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for y in 0..self.map.height {
            for x in 0..self.map.width {
                let cell = self.map.get(x, y);
                let is_highlight = self
                    .hightlights
                    .iter()
                    .any(|f| f.0 == x as _ && f.1 == y as _);
                if is_highlight {
                    write!(f, "x")?
                } else {
                    write!(f, "{}", cell.unwrap())?
                }
            }
            writeln!(f)?
        }

        write!(f, "")
    }
}

fn parse(input: &str) -> Result<Map> {
    fn parse_line(input: &str) -> IResult<&str, Vec<u8>> {
        let p_digit = map_res(take(1usize), |x: &str| x.parse::<u8>());
        terminated(many1(p_digit), line_ending)(input)
    }

    let a: (&str, Vec<Vec<u8>>) =
        many1(parse_line)(input).map_err(|e| eyre::eyre!("Error while parsing: {}", e))?;

    Ok(Map {
        height: a.1.len(),
        width: a.1.get(0).unwrap().len(),
        trees: a.1.iter().flatten().copied().collect(),
    })
}

fn trace(map: &Map, start_x: i32, start_y: i32, dir_x: i32, dir_y: i32) -> Vec<(i32, i32)> {
    let mut pos_x = start_x;
    let mut pos_y = start_y;
    let mut last_tree = -1;
    let mut found_trees = vec![];

    loop {
        if let Some(cell) = map.get(pos_x as _, pos_y as _) {
            if (cell as i32) > last_tree {
                found_trees.push((pos_x, pos_y));
            }
            pos_x += dir_x;
            pos_y += dir_y;
            last_tree = i32::max(cell as i32, last_tree);
        } else {
            break found_trees;
        }
    }
}

fn part1(input: &str) -> Result<i32> {
    let map = &parse(input)?;
    let w = map.width - 1;
    let h = map.height - 1;
    let action = vec![
        //  vv
        //  ##
        //  ##
        ((0..=w).collect_vec(), (0..=0).collect_vec(), 0, 1),
        //  ##
        //  ##
        //  ^^
        ((0..=w).rev().collect_vec(), (h..=h).collect_vec(), 0, -1),
        // >##
        // >##
        ((0..=0).collect_vec(), (0..=h).rev().collect_vec(), 1, 0),
        //  ##<
        //  ##<
        ((w..=w).collect_vec(), (0..=h).collect_vec(), -1, 0),
    ];

    let value = action
        .iter()
        .flat_map(|(s_x, s_y, d_x, d_y)| {
            s_x.clone()
                .into_iter()
                .cartesian_product(s_y.clone())
                .map(move |(x, y)| (x, y, d_x, d_y))
        })
        .flat_map(|(x, y, d_x, d_y)| trace(map, x as _, y as _, *d_x, *d_y))
        .sorted()
        .unique()
        .collect_vec();

    Ok(value.len() as i32)
}

fn trace_scenic(map: &Map, start_x: i32, start_y: i32, dir_x: i32, dir_y: i32) -> Vec<(i32, i32)> {
    let mut pos_x = start_x;
    let mut pos_y = start_y;
    let mut found_trees = vec![];
    let this_tree = map.get(start_x as _, start_y as _).unwrap() as i32;

    loop {
        pos_x += dir_x;
        pos_y += dir_y;

        if let Some(cell) = map.get(pos_x as _, pos_y as _) {
            found_trees.push((pos_x, pos_y));

            if (cell as i32) >= this_tree {
                break found_trees;
            }
        } else {
            break found_trees;
        }
    }
}

fn part2(input: &str) -> Result<i32> {
    let map = &parse(input)?;
    let scores = (0..map.width)
        .cartesian_product(0..map.height)
        .map(move |(x, y)| {
            [
                trace_scenic(map, x as _, y as _, 0, -1),
                trace_scenic(map, x as _, y as _, -1, 0),
                trace_scenic(map, x as _, y as _, 0, 1),
                trace_scenic(map, x as _, y as _, 1, 0),
            ]
            .iter()
            .map(|f| f.len())
            .reduce(|acc, f| acc * f)
            .unwrap_or(0usize)
        });

    Ok(scores.max().unwrap() as _)
}

#[cfg(test)]
mod test {
    use super::{part1, part2};

    #[test]
    fn test_one() {
        let input = include_str!("../input/day-8/example.txt");
        let value = part1(input).unwrap();

        assert_eq!(value, 21)
    }

    #[test]
    fn test_two() {
        let input = include_str!("../input/day-8/example.txt");
        let value = part2(input).unwrap();

        assert_eq!(value, 8)
    }
}

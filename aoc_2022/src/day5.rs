use std::{borrow::BorrowMut, collections::VecDeque};

use crate::Solution;
use eyre::{eyre, Result};
use nom::{
    branch::{alt, permutation},
    bytes::complete::{tag, take},
    character::complete::digit0,
    combinator::{map_res, opt},
    error::ErrorKind,
    multi::many0,
    sequence::{delimited, terminated},
    IResult,
};

pub struct Day {}

impl Day {
    pub fn new() -> Self {
        Day {}
    }
}

impl Solution for Day {
    fn run(&self, input_str: &str) -> Result<()> {
        let crane = &parse(input_str)?;

        println!("Part 1: {}\nPart 2: {}", part1(crane)?, part2(crane)?);

        Ok(())
    }
}

#[derive(Debug, Clone)]
struct CraneMove {
    pub count: u8,
    pub from: u8,
    pub to: u8,
}

#[derive(Debug, Clone)]
struct CrateStack {
    pub stacks: Vec<VecDeque<char>>,
}

#[derive(Debug, Clone)]
struct Crane {
    pub moves: Vec<CraneMove>,
    pub stack: CrateStack,
}

fn parse_stack_line(input: &str) -> IResult<&str, &str> {
    let empty = tag("   ");
    let not_empty = delimited(tag("["), take(1usize), tag("]"));
    let numbers = delimited(tag(" "), take(1usize), tag(" "));
    let trimmed = map_res(alt((empty, not_empty, numbers)), |s: &str| {
        Ok::<&str, ErrorKind>(s.trim())
    });
    terminated(trimmed, opt(tag(" ")))(input)
}

fn parse_stack_lines(input: &str) -> IResult<&str, CrateStack> {
    map_res(many0(terminated(many0(parse_stack_line), tag("\n"))), |f| {
        let len = f.len() - 1;
        let mut stacks: Vec<VecDeque<char>> = (0..len).map(|_| VecDeque::from(vec![])).collect();

        for (index, item) in f.into_iter().take(len).flatten().enumerate() {
            if let Some(c) = item.chars().next() {
                if let Some(v) = stacks.get_mut(index % len) {
                    v.borrow_mut().push_back(c);
                }
            }
        }

        Ok::<_, ErrorKind>(CrateStack { stacks })
    })(input)
}

fn parse_digit(input: &str) -> IResult<&str, u8> {
    map_res(digit0, |f: &str| f.parse::<u8>())(input)
}

fn parse_move(input: &str) -> IResult<&str, CraneMove> {
    map_res(
        permutation((
            delimited(tag("move "), parse_digit, tag(" ")),
            delimited(tag("from "), parse_digit, tag(" ")),
            delimited(tag("to "), parse_digit, tag("\n")),
        )),
        |f| {
            Ok::<_, ErrorKind>(CraneMove {
                count: f.0,
                from: f.1,
                to: f.2,
            })
        },
    )(input)
}

fn parse_moves(input: &str) -> IResult<&str, Vec<CraneMove>> {
    many0(parse_move)(input)
}

fn parse(input: &str) -> Result<Crane> {
    let (_, (crate_stack, crane_move)) = permutation((parse_stack_lines, parse_moves))(input)
        .map_err(|f| eyre!("Failed to parse. Err: {}", f))?;
    Ok(Crane {
        moves: crane_move,
        stack: crate_stack,
    })
}

fn part1(crane: &Crane) -> Result<String> {
    let stacks = &mut crane.stack.stacks.clone();
    let moves = &crane.moves;
    for mov in moves.iter() {
        let from = (mov.from - 1) as usize;
        let to = (mov.to - 1) as usize;
        for _i in 0..mov.count {
            let c = stacks
                .get_mut(from)
                .unwrap()
                .borrow_mut()
                .pop_front()
                .unwrap();

            stacks.get_mut(to).unwrap().borrow_mut().push_front(c);
        }
    }

    Ok(stacks
        .iter_mut()
        .map_while(|x| x.front())
        .collect::<String>())
}

fn part2(crane: &Crane) -> Result<String> {
    let stacks = &mut crane.stack.stacks.clone();
    let moves = &crane.moves;
    for mov in moves.iter() {
        let from = (mov.from - 1) as usize;
        let to = (mov.to - 1) as usize;

        let tmp = &mut vec![];
        for _i in 0..mov.count {
            let c = stacks
                .get_mut(from)
                .unwrap()
                .borrow_mut()
                .pop_front()
                .unwrap();

            tmp.push(c);
        }

        for c in tmp.iter_mut().rev() {
            stacks.get_mut(to).unwrap().borrow_mut().push_front(*c);
        }
    }

    Ok(stacks
        .iter_mut()
        .map_while(|x| x.front())
        .collect::<String>())
}

#[test]
fn test_one() {
    let input = include_str!("../input/day-5/example.txt");

    let crane = parse(input).expect("Parsing works.");
    let value = part1(&crane).expect("There is a result.");

    assert_eq!(value, "CMZ");
}

#[test]
fn test_two() {
    let input = include_str!("../input/day-5/example.txt");

    let crane = parse(input).expect("Parsing works.");
    let value = part2(&crane).expect("There is a result.");

    assert_eq!(value, "MCD");
}

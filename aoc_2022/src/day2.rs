use crate::Solution;
use eyre::{bail, eyre, Result};

pub struct Day {}

impl Solution for Day {
    fn run(&self, input_str: &str) -> Result<()> {
        let v = parse(input_str)?
            .iter()
            .map(|&f| Ok((get_score(f.0, f.1)?, cheat(f.0, f.1)?)))
            .collect::<Result<Vec<_>>>()?;

        let part1 = v.iter().map(|f| f.0).sum::<i32>();
        let part2 = v.iter().map(|f| f.1).sum::<i32>();

        println!("Part 1: {}\nPart 2: {}", part1, part2);

        Ok(())
    }
}

fn parse(input_str: &str) -> Result<Vec<(&str, &str)>> {
    input_str
        .lines()
        .map(|f| {
            let x: Vec<&str> = f.trim().split(' ').collect();
            Ok((
                *x.first().ok_or_else(|| eyre!("[006] Invalid data."))?,
                *x.get(1).ok_or_else(|| eyre!("[007] Invalid data."))?,
            ))
        })
        .collect::<Result<_>>()
}

fn get_score(a: &str, b: &str) -> Result<i32> {
    Ok(match b {
        "X" => 1,
        "Y" => 2,
        "Z" => 3,
        _ => bail!("[001] Unknown game move."),
    } + get_match_score(a, b)?)
}

fn get_match_score(a: &str, b: &str) -> Result<i32> {
    match a {
        // Rock
        "A" => match b {
            "X" => Ok(3),
            "Y" => Ok(6),
            "Z" => Ok(0),
            _ => bail!("[002] Unknown game move."),
        },
        // Paper
        "B" => match b {
            "X" => Ok(0),
            "Y" => Ok(3),
            "Z" => Ok(6),
            _ => bail!("[003] Unknown game move."),
        },
        // Scissor
        "C" => match b {
            "X" => Ok(6),
            "Y" => Ok(0),
            "Z" => Ok(3),
            _ => bail!("[004] Unknown game move."),
        },
        _ => bail!("[005] Unknown game move."),
    }
}

fn cheat(a: &str, b: &str) -> Result<i32> {
    Ok(match b {
        // Loose
        "X" => match a {
            "A" => get_score(a, "Z")?,
            "B" => get_score(a, "X")?,
            "C" => get_score(a, "Y")?,
            _ => bail!("[008] Unknown game move."),
        },
        // Draw
        "Y" => match a {
            "A" => get_score(a, "X")?,
            "B" => get_score(a, "Y")?,
            "C" => get_score(a, "Z")?,
            _ => bail!("[009] Unknown game move."),
        },
        // Win
        "Z" => match a {
            "A" => get_score(a, "Y")?,
            "B" => get_score(a, "Z")?,
            "C" => get_score(a, "X")?,
            _ => bail!("[010] Unknown game move."),
        },
        _ => bail!("[011] Unknown game move."),
    })
}

#[test]
fn test_one() -> Result<()> {
    let input_str = "A Y\nB X\nC Z";
    let value = parse(input_str)
        .expect("Failed to parse.")
        .iter()
        .map(|&f| get_score(f.0, f.1))
        .collect::<Result<Vec<_>>>()?
        .iter()
        .sum::<i32>();

    assert_eq!(15, value);

    Ok(())
}

#[test]
fn test_two() -> Result<()> {
    let input_str = "A Y\nB X\nC Z";
    let value = parse(input_str)
        .expect("Failed to parse.")
        .iter()
        .map(|&f| cheat(f.0, f.1))
        .collect::<Result<Vec<_>>>()?
        .iter()
        .sum::<i32>();

    assert_eq!(12, value);

    Ok(())
}

use crate::Solution;
use eyre::Result;
use itertools::Itertools;
use std::collections::HashMap;

pub struct Day {
    char_map: HashMap<char, i32>,
}

impl Day {
    pub fn new() -> Self {
        Day {
            char_map: get_chars(),
        }
    }
}

impl Solution for Day {
    fn run(&self, input_str: &str) -> Result<()> {
        let input = parse(&self.char_map, input_str)?;

        println!("Part 1: {}\nPart 2: {}", part1(&input)?, part2(&input)?);

        Ok(())
    }
}

fn parse(map: &HashMap<char, i32>, input_str: &str) -> Result<Vec<Vec<i32>>> {
    Ok(input_str
        .trim()
        .lines()
        .map(|f| f.chars().map(|x| map[&x]).collect())
        .collect())
}

fn part1(slice: &[Vec<i32>]) -> Result<i32> {
    Ok(slice
        .iter()
        .flat_map(|f| {
            let len = f.len() / 2;
            let x = f.iter().take(len).sorted().dedup();
            let y = f.iter().skip(len).sorted().dedup();
            x.chain(y).duplicates()
        })
        .sum::<i32>())
}

fn part2(slice: &[Vec<i32>]) -> Result<i32> {
    Ok(slice
        .iter()
        .chunks(3)
        .into_iter()
        .flat_map(|x| {
            x.map(|y| y.iter().sorted().dedup().collect::<Vec<&i32>>())
                .reduce(|acc, item| {
                    acc.iter()
                        .chain(item.iter())
                        .sorted()
                        .duplicates()
                        .copied()
                        .collect()
                })
                .unwrap()
        })
        .sum())
}

fn get_chars() -> HashMap<char, i32> {
    ('a'..'{')
        .chain('A'..'[')
        .enumerate()
        .map(|(i, x)| (x, (i + 1) as i32))
        .collect()
}

#[test]
fn test_one() {
    let char_map = get_chars();

    assert_eq!(char_map[&'a'], 1);
    assert_eq!(char_map[&'z'], 26);
    assert_eq!(char_map[&'A'], 27);
    assert_eq!(char_map[&'Z'], 52);
}

#[test]
fn test_two() {
    let input_str = include_str!("../input/day-3/example.txt");
    let char_map = get_chars();

    let value = parse(&char_map, input_str).expect("Expected result.");
    let value = part1(&value).expect("Expected result.");

    assert_eq!(157, value);
}

#[test]
fn test_three() {
    let input_str = include_str!("../input/day-3/example.txt");
    let char_map = get_chars();

    let value = parse(&char_map, input_str).expect("Expected result.");
    let value = part2(&value).expect("Expected result.");

    assert_eq!(70, value);
}

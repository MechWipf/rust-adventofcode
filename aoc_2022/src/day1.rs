use crate::Solution;
use eyre::{eyre, Result};
use itertools::Itertools;

pub struct Day {}

impl Solution for Day {
    fn run(&self, input_str: &str) -> Result<()> {
        let values = input_str
            .split("\n\n")
            .map(|f| {
                f.lines()
                    .map(|g| g.parse::<i32>().expect("invalid value"))
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();

        println!(
            "Part 1: {}\nPart 2: {}",
            values
                .iter()
                .map(|f| f.iter().sum::<i32>())
                .max()
                .ok_or_else(|| eyre!("Empty result."))?,
            values
                .iter()
                .map(|f| f.iter().sum::<i32>())
                .sorted_by(|a, b| { Ord::cmp(b, a) })
                .take(3)
                .sum::<i32>()
        );

        Ok(())
    }
}

use crate::Solution;
use eyre::Result;
use itertools::Itertools;

pub struct Day {}

impl Day {
    pub fn new() -> Self {
        Day {}
    }
}

impl Solution for Day {
    fn run(&self, input_str: &str) -> Result<()> {
        let input = parse(input_str)?;
        println!("Part 1: {}\nPart 2: {}", part1(&input), part2(&input));

        Ok(())
    }
}

#[derive(Debug)]
struct WorkItem(i32, i32);

fn parse(input_str: &str) -> Result<Vec<WorkItem>> {
    let a = input_str
        .lines()
        .flat_map(|x| x.split(','))
        .flat_map(|x| x.split('-'))
        .map(|x| x.parse::<i32>().unwrap())
        .tuples()
        .map(|x: (i32, i32)| WorkItem(x.0, x.1))
        .collect();
    Ok(a)
}

fn does_include(a: &WorkItem, b: &WorkItem) -> bool {
    a.0 >= b.0 && a.1 <= b.1
}

fn does_overlap(a: &WorkItem, b: &WorkItem) -> bool {
    a.1 >= b.0
}

fn part1(slice: &[WorkItem]) -> i32 {
    slice
        .iter()
        .tuples()
        .map(|x: (&WorkItem, &WorkItem)| (does_include(x.0, x.1) || does_include(x.1, x.0)) as i32)
        .sum::<i32>()
}

fn part2(slice: &[WorkItem]) -> i32 {
    slice
        .iter()
        .tuples()
        .map(|x: (&WorkItem, &WorkItem)| {
            if x.0 .0 < x.1 .0 {
                does_overlap(x.0, x.1) as i32
            } else {
                does_overlap(x.1, x.0) as i32
            }
        })
        .sum::<i32>()
}

#[test]
fn test_one() {
    let input_str = include_str!("../input/day-4/example.txt");
    let value = parse(input_str).expect("result");
    let value = part1(&value);

    assert_eq!(2, value);
}

#[test]
fn test_two() {
    let input_str = include_str!("../input/day-4/example.txt");
    let value = parse(input_str).expect("result");
    let value = part2(&value);

    assert_eq!(4, value);
}

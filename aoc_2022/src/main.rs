use std::fs::read_to_string;
use std::path::Path;

use clap::Parser;
use eyre::{eyre, Context, Result};

#[derive(Parser, Debug)]
#[command(
    author="MechWipf <MechWipf@protonmail.com>",
    version="1.0.0",
    about="Advent of Code.. what else",
    long_about=None)]
struct Args {
    #[arg(short, long, help = "Specify which day to run")]
    day: String,

    #[arg(short, long, help = "Puzzel input to run on")]
    input: String,
}

fn create_day(day: i32) -> Result<Box<dyn Solution>> {
    match day {
        1 => Ok(Box::new(day1::Day {})),
        2 => Ok(Box::new(day2::Day {})),
        3 => Ok(Box::new(day3::Day::new())),
        4 => Ok(Box::new(day4::Day::new())),
        5 => Ok(Box::new(day5::Day::new())),
        6 => Ok(Box::new(day6::Day::new())),
        7 => Ok(Box::new(day7::Day::new())),
        8 => Ok(Box::new(day8::Day::new())),
        _ => Err(eyre!("Unknown day.")),
    }
}

fn main() -> Result<()> {
    let args = Args::parse();

    let day = args.day.parse::<i32>().wrap_err("Failed to parse day.")?;

    let folder = Path::new(args.input.as_str());
    let mut input = Path::join(folder, format!("day-{}", day));
    input.push("input.txt");

    let input_str = read_to_string(input).wrap_err("Failed to read input.txt")?;
    let day_to_run = create_day(day)?;
    day_to_run.run(&input_str)?;

    Ok(())
}

trait Solution {
    fn run(&self, input_str: &str) -> Result<()>;
}

mod day1;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;

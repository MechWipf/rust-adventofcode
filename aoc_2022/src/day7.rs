use crate::Solution;
use eyre::Result;
use itertools::Itertools;
use std::{collections::HashMap, path::PathBuf};

pub struct Day {}
impl Day {
    pub fn new() -> Day {
        Day {}
    }
}

impl Solution for Day {
    fn run(&self, input_str: &str) -> Result<()> {
        println!(
            "Part 1: {}
Part 2: {}",
            part1(input_str)?,
            part2(input_str)?
        );

        Ok(())
    }
}

#[derive(Debug)]
enum ParsedType<'a> {
    CdCommand(&'a str),
    LsCommand(),
    File(i32, &'a str),
    Dir(&'a str),
}

fn parse_command(input: &str) -> nom::IResult<&str, ParsedType> {
    use nom::{branch, bytes, character, combinator, error, sequence};

    let p_cmd = nom::sequence::delimited(
        character::complete::char(' '),
        character::complete::alpha1,
        nom::combinator::opt(character::complete::char(' ')),
    );

    combinator::map_res(
        sequence::terminated(
            sequence::preceded(
                bytes::complete::tag("$"),
                branch::permutation((p_cmd, combinator::opt(bytes::complete::take_until("\n")))),
            ),
            character::complete::line_ending,
        ),
        |f| match f.0 {
            "cd" => Ok::<_, error::ErrorKind>(ParsedType::CdCommand(f.1.unwrap())),
            "ls" => Ok::<_, error::ErrorKind>(ParsedType::LsCommand()),
            _ => unreachable!(),
        },
    )(input)
}

fn parse_output(input: &str) -> nom::IResult<&str, ParsedType> {
    use nom::{branch, bytes, character, combinator, error, sequence};

    fn p_file_name(i: &str) -> nom::IResult<&str, &str> {
        nom::sequence::delimited(
            character::complete::char(' '),
            bytes::complete::take_until("\n"),
            nom::combinator::opt(character::complete::char(' ')),
        )(i)
    }

    let p_dir = sequence::preceded(
        bytes::complete::tag("dir "),
        bytes::complete::take_until("\n"),
    );
    let p_dir = combinator::map_res(p_dir, |f| Ok::<_, error::ErrorKind>(ParsedType::Dir(f)));

    let p_file = combinator::map_res(
        sequence::pair(
            combinator::map_res(bytes::complete::take_until(" "), |f: &str| f.parse::<i32>()),
            p_file_name,
        ),
        |f| Ok::<_, error::ErrorKind>(ParsedType::File(f.0, f.1)),
    );

    sequence::terminated(
        branch::alt((p_dir, p_file)),
        character::complete::line_ending,
    )(input)
}

fn parse(input: &str) -> Result<Vec<ParsedType>> {
    let (_, res) = nom::multi::many1(nom::branch::alt((parse_command, parse_output)))(input)
        .map_err(|e| eyre::eyre!("Failed to parse with error: {}", e))?;
    Ok(res)
}

fn construct_directory(input: &str) -> Result<HashMap<String, i32>> {
    let mut dict = HashMap::<String, i32>::new();

    {
        let c_path = &mut PathBuf::from("/");
        let dict = &mut dict;
        dict.insert(c_path.to_str().unwrap().to_owned(), 0);

        for r in parse(input)? {
            match r {
                ParsedType::CdCommand(path) => {
                    if path == ".." {
                        c_path.pop();
                    } else {
                        c_path.push(path);
                    }
                }
                ParsedType::LsCommand() => (),
                ParsedType::File(size, _) => {
                    let p = &mut c_path.clone();

                    dict.entry(c_path.to_str().unwrap().to_string())
                        .and_modify(|x| *x += size);

                    if p.as_os_str() != "/" {
                        loop {
                            p.pop();

                            dict.entry(p.to_str().unwrap().to_string())
                                .and_modify(|x| *x += size);

                            if p.as_os_str() == "/" {
                                break;
                            }
                        }
                    }
                }
                ParsedType::Dir(dir) => {
                    let p = c_path.join(dir);
                    dict.entry(p.to_str().unwrap().to_owned()).or_insert(0);
                }
            }
        }
    }

    Ok(dict)
}

fn part1(input: &str) -> Result<i32> {
    let dict = &construct_directory(input)?;

    Ok(dict
        .iter()
        .filter(|(_, size)| **size < 100000)
        .map(|(_, x)| *x)
        .sum::<i32>())
}

fn part2(input: &str) -> Result<i32> {
    let dict = &construct_directory(input)?;

    let free_space = 70000000 - dict["/"];
    let space_to_free = 30000000 - free_space;

    Ok(dict
        .iter()
        .filter(|(_, size)| **size >= space_to_free)
        .map(|(_, x)| *x)
        .sorted()
        .next()
        .unwrap())
}

#[cfg(test)]
mod test {
    use super::{part1, part2};

    #[test]
    fn test_one() {
        let input = include_str!("../input/day-7/example.txt");
        let value = part1(input).unwrap();

        assert_eq!(value, 95437)
    }

    #[test]
    fn test_two() {
        let input = include_str!("../input/day-7/example.txt");
        let value = part2(input).unwrap();

        assert_eq!(value, 24933642)
    }
}

set dotenv-load

default:
    @just --list

run YEAR DAY:
    #!/usr/bin/env fish
    set euxo pipefail

    set project "aoc_{{ YEAR }}"
    set input_folder "$project/input"

    mkdir -p $input_folder
    if test -e "$input_folder/day-{{ DAY }}/input.txt"
    else
        curl -A "mechwipf@protonmail.com via curl" \
            --cookie "session=$SESSION_TOKEN" \
            --create-dirs \
            https://adventofcode.com/{{ YEAR }}/day/{{ DAY }}/input \
            -o "$input_folder/day-{{ DAY }}/input.txt"
    end

    cargo build --release
    time target/release/aoc_{{ YEAR }} --day {{ DAY }} --input $input_folder

dbg YEAR DAY BACKTRACE="0":
    #!/usr/bin/env fish
    set euxo pipefail

    set project "aoc_{{ YEAR }}"
    set input_folder "$project/input"

    mkdir -p $input_folder
    if test -e "$input_folder/day-{{ DAY }}/input.txt"
    else
        curl -A "mechwipf@protonmail.com via curl" \
            --cookie "session=$SESSION_TOKEN" \
            --create-dirs \
            https://adventofcode.com/{{ YEAR }}/day/{{ DAY }}/input \
            -o "$input_folder/day-{{ DAY }}/input.txt"
    end

    time env RUST_LIB_BACKTRACE={{ BACKTRACE }} cargo run --package aoc_{{ YEAR }} -- --day {{ DAY }} --input $input_folder

compile-windows:
    podman run --rm --name rust -v {{ justfile_directory() }}:/workspace:z -w /workspace rust sh -c "\
        set eux;\
        apt-get update;\
        apt-get install mingw-w64 -y;\
        rustup target add x86_64-pc-windows-gnu;\
        cargo build --target=x86_64-pc-windows-gnu --release"
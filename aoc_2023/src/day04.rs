use color_eyre::eyre::{eyre, Context};
use itertools::Itertools;

use crate::Solution;

pub struct Day {}

impl Solution for Day {
    fn run(&self, input_str: &str) -> color_eyre::Result<()> {
        let _ = input_str;
        let result1 = result1(input_str)?;
        let result2 = result2(input_str)?;
        println!("Part 1: {}\nPart 2: {}", result1, result2);
        Ok(())
    }
}

fn result1(input_str: &str) -> color_eyre::Result<String> {
    let solution: u32 = input_str
        .lines()
        .map(|line| {
            let (_, rest) = line
                .split_once(':')
                .ok_or_else(|| eyre!("failed to get rest"))?;
            let (winners, own) = rest
                .split_once('|')
                .ok_or_else(|| eyre!("failed to get winners and own"))?;
            let winners = winners
                .split(' ')
                .filter(|x| !x.is_empty())
                .map(|x| x.parse())
                .collect::<Result<Vec<u32>, _>>()?;
            let own = own
                .split(' ')
                .filter(|x| !x.is_empty())
                .map(|x| x.parse())
                .collect::<Result<Vec<u32>, _>>()?;

            let matches = own
                .iter()
                .filter(|own| winners.contains(own))
                .fold(0, |acc, _| if acc == 0 { 1 } else { acc * 2 });

            Ok(matches)
        })
        .collect::<color_eyre::Result<Vec<u32>>>()?
        .iter()
        .sum();

    Ok(solution.to_string())
}

fn result2(input_str: &str) -> color_eyre::Result<String> {
    let cards: Vec<_> = input_str
        .lines()
        .map(|line| {
            let (card, rest) = line
                .split_once(':')
                .ok_or_else(|| eyre!("failed to split into card and rest"))?;
            let (_, id) = card
                .split_once(' ')
                .ok_or_else(|| eyre!("failed to get id"))?;
            let (winners, own) = rest
                .split_once('|')
                .ok_or_else(|| eyre!("failed to split into winners and own"))?;
            let winners: Vec<u32> = winners
                .split(' ')
                .filter(|x| !x.is_empty())
                .map(|x| x.parse::<u32>())
                .collect::<Result<_, _>>()?;
            let own: Vec<u32> = own
                .split(' ')
                .filter(|x| !x.is_empty())
                .map(|x| x.parse::<u32>())
                .collect::<Result<_, _>>()?;

            let id: u32 = id
                .trim()
                .parse::<u32>()
                .wrap_err_with(|| "failed to parse id")?
                - 1;

            let cards_won = own
                .iter()
                .filter(|own| winners.contains(own))
                .enumerate()
                .map(|(x, _)| id + (x as u32) + 1)
                .collect_vec();

            Ok((id, cards_won))
        })
        .collect::<color_eyre::Result<_>>()?;

    let card_values = &mut vec![1u64; cards.len()];

    for (id, cards_won) in cards.iter() {
        let this_value = *card_values
            .get(*id as usize)
            .ok_or_else(|| eyre!("Something broke {}", id))?;

        cards_won.iter().for_each(|card| {
            if let Some(x) = card_values.get_mut(*card as usize) {
                *x += this_value;
            }
        });
    }

    Ok(card_values.iter().sum::<u64>().to_string())
}

#[cfg(test)]
mod test {
    const EXAMPLE: &str = r#"Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11
"#;

    #[test]
    fn test_one() {
        let res = super::result1(EXAMPLE).unwrap();
        assert_eq!(res, "13");
    }

    #[test]
    fn test_two() {
        let res = super::result2(EXAMPLE).unwrap();
        assert_eq!(res, "30");
    }
}

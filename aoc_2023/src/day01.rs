use crate::Solution;
use itertools::Itertools;
use nom::FindSubstring;

pub struct Day {}

impl Solution for Day {
    fn run(&self, input_str: &str) -> color_eyre::Result<()> {
        let first_sol = input_str
            .lines()
            .map(|x| {
                x.chars()
                    .filter(|y| y.is_numeric())
                    .map(|y| y as i32 - 48)
                    .collect_vec()
            })
            .filter(|x| !x.is_empty())
            .map(|x| 10 * x.first().unwrap() + x.last().unwrap())
            .sum::<i32>();

        let sec_sol = input_str
            .lines()
            .map(|x| dbg!(10 * find_first_digit(x) + find_last_digit(x)))
            .sum::<i32>();

        println!("Part 1: {}\nPart 2: {}", first_sol, sec_sol);

        Ok(())
    }
}

fn find_first_digit(data: &str) -> i32 {
    let finds = [
        (1, data.find_substring("one")),
        (2, data.find_substring("two")),
        (3, data.find_substring("three")),
        (4, data.find_substring("four")),
        (5, data.find_substring("five")),
        (6, data.find_substring("six")),
        (7, data.find_substring("seven")),
        (8, data.find_substring("eight")),
        (9, data.find_substring("nine")),
        (1, data.find_substring("1")),
        (2, data.find_substring("2")),
        (3, data.find_substring("3")),
        (4, data.find_substring("4")),
        (5, data.find_substring("5")),
        (6, data.find_substring("6")),
        (7, data.find_substring("7")),
        (8, data.find_substring("8")),
        (9, data.find_substring("9")),
    ];

    finds
        .iter()
        .filter(|x| x.1.is_some())
        .map(|(x, y)| (*x, y.unwrap()))
        .min_by(|x, y| x.1.cmp(&y.1))
        .unwrap()
        .0
}

fn find_last_digit(data: &str) -> i32 {
    let d = data.chars().rev().collect::<String>();
    let data = d.as_str();

    let finds = [
        (1, data.find_substring("eno")),
        (2, data.find_substring("owt")),
        (3, data.find_substring("eerht")),
        (4, data.find_substring("ruof")),
        (5, data.find_substring("evif")),
        (6, data.find_substring("xis")),
        (7, data.find_substring("neves")),
        (8, data.find_substring("thgie")),
        (9, data.find_substring("enin")),
        (1, data.find_substring("1")),
        (2, data.find_substring("2")),
        (3, data.find_substring("3")),
        (4, data.find_substring("4")),
        (5, data.find_substring("5")),
        (6, data.find_substring("6")),
        (7, data.find_substring("7")),
        (8, data.find_substring("8")),
        (9, data.find_substring("9")),
    ];

    finds
        .iter()
        .filter(|x| x.1.is_some())
        .map(|(x, y)| (*x, y.unwrap()))
        .min_by(|x, y| x.1.cmp(&y.1))
        .unwrap()
        .0
}

#[test]
fn test_day_example1() -> color_eyre::Result<()> {
    let input_str = r#"1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet
"#;
    let day = super::day01::Day {};
    day.run(input_str)
}

#[test]
fn test_day_example2() -> color_eyre::Result<()> {
    let input_str = r#"two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen
"#;
    let day = super::day01::Day {};
    day.run(input_str)
}

#[test]
fn test_phonic() -> color_eyre::Result<()> {
    let data = "eightwothree";
    assert_eq!(find_first_digit(data), 8);
    assert_eq!(find_last_digit(data), 3);
    Ok(())
}

use std::fs::read_to_string;
use std::path::Path;

use clap::Parser;
use color_eyre::eyre::Context;
use color_eyre::Result;

#[derive(Parser, Debug)]
#[command(
    author="MechWipf <MechWipf@protonmail.com>",
    version="1.0.0",
    about="Advent of Code.. what else",
    long_about=None)]
struct Args {
    #[arg(short, long, help = "Specify which day to run")]
    day: i32,

    #[arg(short, long, help = "Puzzel input to run on")]
    input: String,
}

fn create_day(day: i32) -> Result<Box<dyn Solution>> {
    match day {
        1 => Ok(Box::new(day01::Day {})),
        2 => Ok(Box::new(day02::Day {})),
        3 => Ok(Box::new(day03::Day {})),
        4 => Ok(Box::new(day04::Day {})),
        5 => Ok(Box::new(day05::Day {})),
        6 => Ok(Box::new(day06::Day {})),
        7 => Ok(Box::new(day07::Day {})),
        8 => Ok(Box::new(day08::Day {})),
        9 => Ok(Box::new(day09::Day {})),
        10 => Ok(Box::new(day10::Day {})),
        11 => Ok(Box::new(day11::Day {})),
        12 => Ok(Box::new(day12::Day {})),
        13 => Ok(Box::new(day13::Day {})),
        14 => Ok(Box::new(day14::Day {})),
        15 => Ok(Box::new(day15::Day {})),
        16 => Ok(Box::new(day16::Day {})),
        17 => Ok(Box::new(day17::Day {})),
        18 => Ok(Box::new(day18::Day {})),
        19 => Ok(Box::new(day19::Day {})),
        20 => Ok(Box::new(day20::Day {})),
        21 => Ok(Box::new(day21::Day {})),
        22 => Ok(Box::new(day22::Day {})),
        23 => Ok(Box::new(day23::Day {})),
        24 => Ok(Box::new(day24::Day {})),
        25 => Ok(Box::new(day25::Day {})),
        _ => todo!(),
    }
}

fn main() -> Result<()> {
    color_eyre::install()?;
    let args = Args::parse();

    let day = args.day;

    let folder = Path::new(args.input.as_str());
    let mut input = Path::join(folder, format!("day-{}", day));
    input.push("input.txt");

    let input_str = read_to_string(input).wrap_err("Failed to read input.txt")?;
    let day_to_run = create_day(day)?;
    day_to_run.run(&input_str)?;

    Ok(())
}

trait Solution {
    fn run(&self, input_str: &str) -> Result<()>;
}

mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
mod day07;
mod day08;
mod day09;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
mod day17;
mod day18;
mod day19;
mod day20;
mod day21;
mod day22;
mod day23;
mod day24;
mod day25;

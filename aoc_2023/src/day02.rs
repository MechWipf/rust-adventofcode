use itertools::Itertools;

use crate::Solution;

pub struct Day {}

impl Solution for Day {
    fn run(&self, input_str: &str) -> color_eyre::Result<()> {
        let _ = input_str;
        let result1 = result1(input_str)?;
        let result2 = result2(input_str)?;
        println!("Part 1: {}\nPart 2: {}", result1, result2);
        Ok(())
    }
}

fn result1(input_str: &str) -> color_eyre::Result<String> {
    let bag = (12, 13, 14);

    let data = input_str
        .lines()
        .map(|s| {
            let s = s.split_once(':').unwrap();
            (s.0.replace("Game ", "").parse::<i32>().unwrap(), s.1.trim())
        })
        .map(|(id, sets)| {
            (
                id,
                sets.split(';')
                    .map(|x| x.trim().split(',').map(|y| y.trim()).collect::<Vec<_>>())
                    .map(|x| set_to_tuple(x.as_slice()))
                    .collect::<Vec<_>>(),
            )
        })
        .filter(|(_, sets)| {
            sets.iter()
                .all(|x| bag.0 >= x.0 && bag.1 >= x.1 && bag.2 >= x.2)
        })
        .map(|x| x.0)
        .sum::<i32>();

    Ok(data.to_string())
}

fn result2(input_str: &str) -> color_eyre::Result<String> {
    let data: i32 = input_str
        .lines()
        .map(|s| {
            let s = s.split_once(':').unwrap();
            (s.0.replace("Game ", "").parse::<i32>().unwrap(), s.1.trim())
        })
        .map(|(id, sets)| {
            (
                id,
                sets.split(';')
                    .map(|x| x.trim().split(',').map(|y| y.trim()).collect::<Vec<_>>())
                    .map(|x| set_to_tuple(x.as_slice()))
                    .collect::<Vec<_>>(),
            )
        })
        .map(|(id, sets)| {
            let red = sets.iter().max_by(|x, y| x.0.cmp(&y.0)).unwrap().0;
            let green = sets.iter().max_by(|x, y| x.1.cmp(&y.1)).unwrap().1;
            let blue = sets.iter().max_by(|x, y| x.2.cmp(&y.2)).unwrap().2;
            (id, (red, green, blue))
        })
        .map(|(_, (r, g, b))| r * g * b)
        .sum();

    Ok(data.to_string())
}

fn set_to_tuple(set: &[&str]) -> (i32, i32, i32) {
    let data = set.iter().map(|x| _split(x)).collect_vec();

    let red = data
        .iter()
        .filter(|(_, colour)| colour.eq(&"red"))
        .map(|(q, _)| q)
        .max()
        .unwrap_or(&0);

    let green = data
        .iter()
        .filter(|(_, colour)| colour.eq(&"green"))
        .map(|(q, _)| q)
        .max()
        .unwrap_or(&0);

    let blue = data
        .iter()
        .filter(|(_, colour)| colour.eq(&"blue"))
        .map(|(q, _)| q)
        .max()
        .unwrap_or(&0);

    fn _split(s: &str) -> (i32, &str) {
        let s = s.split_once(' ').unwrap();
        (s.0.parse().unwrap(), s.1)
    }

    (*red, *green, *blue)
}

#[test]
fn test_result_1() -> color_eyre::Result<()> {
    let input = r#"Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
"#;

    let res = result1(input)?;
    assert_eq!(res, "8");

    Ok(())
}

#[test]
fn test_result_2() -> color_eyre::Result<()> {
    let input = r#"Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
"#;

    let res = result2(input)?;
    assert_eq!(res, "2286");

    Ok(())
}

use std::collections::HashMap;

use color_eyre::eyre::eyre;
use itertools::Itertools;
use nom::InputIter;

use crate::Solution;

pub struct Day {}

impl Solution for Day {
    fn run(&self, input_str: &str) -> color_eyre::Result<()> {
        let card_value: HashMap<char, i32> = get_card_values();
        let result1 = result1(input_str).unwrap_or_else(|_| "-1".to_string());
        let result2 = result2(input_str).unwrap_or_else(|_| "-1".to_string());
        println!("Part 1: {}\nPart 2: {}", result1, result2);
        Ok(())
    }
}

fn get_card_values() -> HashMap<char, i32> {
    "23456789TJQKA"
        .iter_elements()
        .enumerate()
        .map(|(idx, c)| (c, (idx + 1) as i32))
        .collect()
}

fn get_card_joker_values() -> HashMap<char, i32> {
    "J23456789TQKA"
        .iter_elements()
        .enumerate()
        .map(|(idx, c)| (c, (idx + 1) as i32))
        .collect()
}

fn result1(input_str: &str) -> color_eyre::Result<String> {
    let card_value = &get_card_values();

    let hands = input_str
        .lines()
        .map(|l| {
            l.split_once(' ')
                .ok_or_else(|| eyre!("expcted ' ' to split"))
        })
        .map_ok(|(x, y)| Ok((x, y.parse::<i32>()?)))
        .flatten()
        .collect::<color_eyre::Result<Vec<_>>>()?;

    let sol = hands
        .iter()
        .map(|(cards, bid)| {
            (
                cards,
                if five_of_a_kind(cards) {
                    7
                } else if four_of_a_kind(cards) {
                    6
                } else if full_house(cards) {
                    5
                } else if three_of_a_kind(cards) {
                    4
                } else if two_pair(cards) {
                    3
                } else if one_pair(cards) {
                    2
                } else {
                    1
                },
                second_order(cards, card_value),
                bid,
            )
        })
        .sorted_by(|(_, a1, a2, _), (_, b1, b2, _)| Ord::cmp(&(*a1, *a2), &(*b1, *b2)))
        .enumerate()
        .map(|(idx, a)| (idx as i32 + 1, a))
        .collect_vec();

    let sol = sol
        .iter()
        .map(|(idx, (_, _, _, bid))| *bid * *idx)
        .sum::<i32>();

    Ok(sol.to_string())
}

fn result2(input_str: &str) -> color_eyre::Result<String> {
    let card_value = &get_card_joker_values();

    let hands = input_str
        .lines()
        .map(|l| {
            l.split_once(' ')
                .ok_or_else(|| eyre!("expcted ' ' to split"))
        })
        .map_ok(|(x, y)| Ok((x, y.parse::<i32>()?)))
        .flatten()
        .collect::<color_eyre::Result<Vec<_>>>()?;

    let sol = hands
        .iter()
        .map(|(cards, bid)| {
            (
                cards,
                {
                    let cards = &convert_joker(cards);
                    if five_of_a_kind(cards) {
                        7
                    } else if four_of_a_kind(cards) {
                        6
                    } else if full_house(cards) {
                        5
                    } else if three_of_a_kind(cards) {
                        4
                    } else if two_pair(cards) {
                        3
                    } else if one_pair(cards) {
                        2
                    } else {
                        1
                    }
                },
                second_order(cards, card_value),
                bid,
            )
        })
        .sorted_by(|(_, a1, a2, _), (_, b1, b2, _)| Ord::cmp(&(*a1, *a2), &(*b1, *b2)))
        .enumerate()
        .map(|(idx, a)| (idx as i32 + 1, a))
        .collect_vec();

    let sol = sol
        .iter()
        .map(|(idx, (_, _, _, bid))| *bid * *idx)
        .sum::<i32>();

    Ok(sol.to_string())
}

fn convert_joker(cards: &str) -> String {
    let dupes = cards
        .iter_elements()
        .sorted()
        .dedup_by_with_count(|x, y| x.eq(y))
        .sorted_by(|a, b| Ord::cmp(&b.0, &a.0))
        .collect_vec();

    let first = dupes.iter().find(|(_, card)| *card != 'J');

    if let Some((_, card)) = first {
        cards.replace('J', &card.to_string())
    } else {
        cards.to_string()
    }
}

fn five_of_a_kind(cards: &str) -> bool {
    cards.iter_elements().all_equal()
}

fn four_of_a_kind(cards: &str) -> bool {
    let dupes = cards
        .iter_elements()
        .sorted()
        .dedup_by_with_count(|x, y| x.eq(y))
        .sorted_by(|a, b| Ord::cmp(&b.0, &a.0))
        .collect_vec();
    dupes.len() == 2 && dupes[0].0 == 4 && dupes[1].0 == 1
}

fn full_house(cards: &str) -> bool {
    let dupes = cards
        .iter_elements()
        .sorted()
        .dedup_by_with_count(|x, y| x.eq(y))
        .sorted_by(|a, b| Ord::cmp(&b.0, &a.0))
        .collect_vec();
    dupes.len() == 2 && dupes[0].0 == 3 && dupes[1].0 == 2
}

fn three_of_a_kind(cards: &str) -> bool {
    let dupes = cards
        .iter_elements()
        .sorted()
        .dedup_by_with_count(|x, y| x.eq(y))
        .sorted_by(|a, b| Ord::cmp(&b.0, &a.0))
        .collect_vec();
    dupes.len() == 3 && dupes[0].0 == 3
}

fn two_pair(cards: &str) -> bool {
    let dupes = cards
        .iter_elements()
        .sorted()
        .dedup_by_with_count(|x, y| x.eq(y))
        .sorted_by(|a, b| Ord::cmp(&b.0, &a.0))
        .collect_vec();
    dupes.len() == 3 && dupes[0].0 == 2 && dupes[1].0 == 2
}

fn one_pair(cards: &str) -> bool {
    let dupes = cards
        .iter_elements()
        .sorted()
        .dedup_by_with_count(|x, y| x.eq(y))
        .sorted_by(|a, b| Ord::cmp(&b.0, &a.0))
        .collect_vec();
    dupes.len() > 1 && dupes[0].0 == 2
}

fn second_order(cards: &str, card_value: &HashMap<char, i32>) -> i64 {
    cards
        .iter_elements()
        .fold(0, |acc, c| (acc * 16) + card_value[&c] as i64)
}

#[cfg(test)]
mod test {
    const EXAMPLE: &str = r#"32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483
"#;

    #[test]
    fn test_one() {
        color_eyre::install().unwrap();
        let res = super::result1(EXAMPLE).unwrap();
        assert_eq!(res, "6440");
    }

    #[test]
    fn test_two() {
        let res = super::result2(EXAMPLE).unwrap();
        assert_eq!(res, "5905");
    }
}

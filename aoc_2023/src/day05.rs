use color_eyre::eyre::eyre;
use itertools::Itertools;
use rayon::iter::{IntoParallelRefIterator, ParallelIterator};

use crate::Solution;

pub struct Day {}

impl Solution for Day {
    fn run(&self, input_str: &str) -> color_eyre::Result<()> {
        let _ = input_str;
        let result1 = result1(input_str)?;
        let result2 = result2(input_str)?;
        println!("Part 1: {}\nPart 2: {}", result1, result2);
        Ok(())
    }
}

fn result1(input_str: &str) -> color_eyre::Result<String> {
    let mut seeds: Vec<u64> = Vec::new();
    let mut transitions = Vec::new();
    let mut current_transition = None;

    for line in input_str.lines() {
        if line.starts_with("seeds:") {
            seeds = line
                .replace("seeds: ", "")
                .split(' ')
                .map(|x| x.parse::<u64>())
                .collect::<Result<_, _>>()?
        } else if line.ends_with("map:") {
            if let Some(ct) = current_transition {
                transitions.push(ct);
            }
            let (ct, _) = line
                .split_once(' ')
                .ok_or_else(|| eyre!("failed parse map name"))?;
            let (fr, to) = ct
                .split_once("-to-")
                .ok_or_else(|| eyre!("failed parse transition name"))?;
            current_transition = Some(((fr, to), Vec::new()));
        } else if !line.is_empty() {
            let (src, dst, length) = line
                .split_whitespace()
                .map(|x| x.parse::<u64>())
                .collect::<Result<Vec<_>, _>>()?
                .iter()
                .cloned()
                .collect_tuple()
                .ok_or_else(|| eyre!("failed to parse transition line"))?;
            if let Some(ct) = current_transition.as_mut() {
                ct.1.push((src..src + length, dst..dst + length))
            }
        }
    }

    if let Some(ct) = current_transition {
        transitions.push(ct);
    }

    let sol = seeds
        .iter()
        .map(|seed| {
            let mut loc = *seed;
            for (_, ranges) in &transitions {
                if let Some(range) = ranges.iter().find(|x| x.1.contains(&loc)) {
                    let diff = loc - range.1.start;
                    loc = range.0.start + diff;
                }
            }

            loc
        })
        .min()
        .ok_or_else(|| eyre!("no minimum found"))?;

    Ok(sol.to_string())
}

fn result2(input_str: &str) -> color_eyre::Result<String> {
    let mut seeds: Vec<_> = Vec::new();
    let mut transitions = Vec::new();
    let mut current_transition = None;

    for line in input_str.lines() {
        if line.starts_with("seeds:") {
            for chunk in &line
                .replace("seeds: ", "")
                .split(' ')
                .map(|x| x.parse::<u64>())
                .chunks(2)
            {
                let (start, length) = chunk
                    .collect::<Result<Vec<_>, _>>()?
                    .iter()
                    .cloned()
                    .collect_tuple()
                    .ok_or_else(|| eyre!("failed to collect seed range"))?;

                seeds.push(start..start + length);
            }
        } else if line.ends_with("map:") {
            if let Some(ct) = current_transition {
                transitions.push(ct);
            }
            let (ct, _) = line
                .split_once(' ')
                .ok_or_else(|| eyre!("failed parse map name"))?;
            let (fr, to) = ct
                .split_once("-to-")
                .ok_or_else(|| eyre!("failed parse transition name"))?;
            current_transition = Some(((fr, to), Vec::new()));
        } else if !line.is_empty() {
            let (src, dst, length) = line
                .split_whitespace()
                .map(|x| x.parse::<u64>())
                .collect::<Result<Vec<_>, _>>()?
                .iter()
                .cloned()
                .collect_tuple()
                .ok_or_else(|| eyre!("failed to parse transition line"))?;
            if let Some(ct) = current_transition.as_mut() {
                ct.1.push((src..src + length, dst..dst + length))
            }
        }
    }

    if let Some(ct) = current_transition {
        transitions.push(ct);
    }

    let sol = seeds
        .par_iter()
        .flat_map(|x| x.clone().collect::<Vec<_>>())
        .map(|seed| {
            let mut loc = seed;
            for (_, ranges) in &transitions {
                if let Some(range) = ranges.iter().find(|x| x.1.contains(&loc)) {
                    let diff = loc - range.1.start;
                    loc = range.0.start + diff;
                }
            }

            loc
        })
        .min()
        .ok_or_else(|| eyre!("no minimum found"))?;

    Ok(sol.to_string())
}

#[cfg(test)]
mod test {
    const EXAMPLE: &str = r#"seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4
"#;

    #[test]
    fn test_one() {
        let res = super::result1(EXAMPLE).unwrap();
        assert_eq!(res, "35");
    }

    #[test]
    fn test_two() {
        let res = super::result2(EXAMPLE).unwrap();
        assert_eq!(res, "46");
    }
}

use crate::Solution;

pub struct Day {}

impl Solution for Day {
    fn run(&self, input_str: &str) -> color_eyre::Result<()> {
        let _ = input_str;
        let result1 = result1(input_str).unwrap_or_else(|_| "-1".to_string());
        let result2 = result2(input_str).unwrap_or_else(|_| "-1".to_string());
        println!("Part 1: {}\nPart 2: {}", result1, result2);
        Ok(())
    }
}

fn result1(_input_str: &str) -> color_eyre::Result<String> {
    todo!()
}

fn result2(_input_str: &str) -> color_eyre::Result<String> {
    todo!()
}

#[cfg(test)]
mod test {
    const EXAMPLE: &str = r#""#;

    #[test]
    fn test_one() {
        let res = super::result1(EXAMPLE).unwrap();
        assert_eq!(res, "");
    }

    #[test]
    fn test_two() {
        let res = super::result2(EXAMPLE).unwrap();
        assert_eq!(res, "");
    }
}

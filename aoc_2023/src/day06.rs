use color_eyre::eyre::eyre;
use itertools::Itertools;

use crate::Solution;

pub struct Day {}

impl Solution for Day {
    fn run(&self, input_str: &str) -> color_eyre::Result<()> {
        let result1 = result1(input_str).unwrap_or_else(|_| "-1".to_string());
        let result2 = result2(input_str).unwrap_or_else(|_| "-1".to_string());
        println!("Part 1: {}\nPart 2: {}", result1, result2);
        Ok(())
    }
}

fn result1(input_str: &str) -> color_eyre::Result<String> {
    let races = {
        let input = &mut input_str.lines();
        let times = &input
            .next()
            .ok_or_else(|| eyre!("input broken"))?
            .split(' ')
            .skip(1)
            .filter(|x| !x.is_empty())
            .map(|x| x.parse::<f32>())
            .collect::<Result<Vec<_>, _>>()?;
        let distances = &input
            .next()
            .ok_or_else(|| eyre!("input broken"))?
            .split(' ')
            .skip(1)
            .filter(|x| !x.is_empty())
            .map(|x| x.parse::<f32>())
            .collect::<Result<Vec<_>, _>>()?;
        times
            .iter()
            .zip(distances)
            .map(|(&x, &y)| (x, y))
            .collect_vec()
    };

    let winning_moves = races
        .iter()
        .map(|(time, distance)| {
            let btn_min = (time - f32::sqrt(f32::powf(*time, 2.0) - 4.0 * *distance)) / 2.0;
            let btn_max = (time + f32::sqrt(f32::powf(*time, 2.0) - 4.0 * *distance)) / 2.0;
            (btn_max - 1.0).ceil() - (btn_min + 1.0).floor() + 1.0
        })
        .collect_vec();

    Ok(winning_moves.iter().product::<f32>().to_string())
}

fn result2(input_str: &str) -> color_eyre::Result<String> {
    let input = &mut input_str.lines();
    let time = &input
        .next()
        .ok_or_else(|| eyre!("input broken"))?
        .split(' ')
        .skip(1)
        .filter(|x| !x.is_empty())
        .collect::<String>()
        .parse::<f32>()?;
    let distance = &input
        .next()
        .ok_or_else(|| eyre!("input broken"))?
        .split(' ')
        .skip(1)
        .filter(|x| !x.is_empty())
        .collect::<String>()
        .parse::<f32>()?;

    let btn_min = (time - f32::sqrt(f32::powf(*time, 2.0) - 4.0 * *distance)) / 2.0;
    let btn_max = (time + f32::sqrt(f32::powf(*time, 2.0) - 4.0 * *distance)) / 2.0;
    let winning_moves = (btn_max - 1.0).ceil() - (btn_min + 1.0).floor() + 1.0;

    Ok(winning_moves.to_string())
}

#[cfg(test)]
mod test {
    const EXAMPLE: &str = r#"Time:      7  15   30
Distance:  9  40  200
"#;

    #[test]
    fn test_one() {
        let res = super::result1(EXAMPLE).unwrap();
        assert_eq!(res, "288");
    }

    #[test]
    fn test_two() {
        let res = super::result2(EXAMPLE).unwrap();
        assert_eq!(res, "71503");
    }
}

use std::collections::HashSet;

use color_eyre::eyre::ContextCompat;
use itertools::Itertools;
use nom::AsChar;

use crate::Solution;

pub struct Day {}

impl Solution for Day {
    fn run(&self, input_str: &str) -> color_eyre::Result<()> {
        let engine = Engine::from_str(input_str).context("engine is totaled")?;
        let result1 = result1(&engine)?;
        let result2 = result2(&engine)?;
        println!("Part 1: {}\nPart 2: {}", result1, result2);
        Ok(())
    }
}

fn result1(engine: &Engine) -> color_eyre::Result<String> {
    let parts = engine
        .parts
        .iter()
        .filter(|part| {
            engine.symbols.iter().any(|symb| {
                (part.y..part.y + part.part.len()).any(|y| symb.area.contains(&(part.x, y)))
            })
        })
        .map(|x| x.part.parse::<i32>().ok())
        .sum::<Option<i32>>()
        .context("failed to sum")?
        .to_string();

    Ok(parts)
}

fn result2(engine: &Engine) -> color_eyre::Result<String> {
    let gear_ratio = engine
        .symbols
        .iter()
        .filter_map(|symb| {
            let parts = engine
                .parts
                .iter()
                .filter(|part| {
                    (part.y..part.y + part.part.len()).any(|y| symb.area.contains(&(part.x, y)))
                })
                .collect_vec();

            if parts.len() == 2 && symb.is_gear {
                Some(parts)
            } else {
                None
            }
        })
        .map(|parts| {
            parts
                .iter()
                .filter_map(|x| x.part.parse::<i32>().ok())
                .product::<i32>()
        })
        .sum::<i32>()
        .to_string();

    Ok(gear_ratio)
}

#[derive(Debug, PartialEq)]
struct Engine {
    pub width: usize,
    pub height: usize,
    pub data: Vec<char>,
    pub parts: Vec<Part>,
    pub symbols: Vec<Symbol>,
}

#[derive(Debug, PartialEq)]
struct Part {
    pub x: usize,
    pub y: usize,
    pub part: String,
}

#[derive(Debug, PartialEq)]
struct Symbol {
    pub x: usize,
    pub y: usize,
    pub is_gear: bool,
    pub area: HashSet<(usize, usize)>,
}

impl Engine {
    fn from_str(input_str: &str) -> Option<Self> {
        let (lines, width, height) = {
            let l = input_str.lines().collect_vec();
            let w = l.first()?.len();
            let h = l.len();
            (l, w, h)
        };

        let data: Vec<char> = lines.iter().flat_map(|x| x.chars()).collect();

        let parts: Vec<Part> = {
            let mut parts = Vec::new();
            let mut index: Option<usize> = None;
            let mut collect: Vec<&char> = Vec::new();
            let mut iterator = data.iter().enumerate();

            let mut push_part = |idx: usize, part: &[&char]| {
                let xy = to_xy(idx, width);
                parts.push(Part {
                    x: xy.0,
                    y: xy.1,
                    part: part.iter().copied().collect::<String>(),
                });
            };

            loop {
                let item = iterator.next();
                if let Some((idx, c)) = item {
                    if c.is_dec_digit() {
                        collect.push(c);

                        if index.is_none() {
                            index = Some(idx);
                        }
                    } else if index.is_some() {
                        push_part(index?, &collect);
                        index = None;
                        collect.clear();
                    }
                } else if !collect.is_empty() {
                    push_part(index?, &collect);
                    index = None;
                    collect.clear();
                } else {
                    break;
                }
            }

            parts
        };

        let symbols: Vec<Symbol> = data
            .iter()
            .enumerate()
            .filter(|(_, x)| !x.is_dec_digit() && **x != '.')
            .map(|(i, x)| {
                let xy = to_xy(i, width);
                Symbol {
                    x: xy.0,
                    y: xy.1,
                    is_gear: *x == '*',
                    area: get_area(xy.0, xy.1),
                }
            })
            .collect_vec();

        Engine {
            width,
            height,
            data,
            parts,
            symbols,
        }
        .into()
    }
}

fn to_xy(index: usize, width: usize) -> (usize, usize) {
    (index / width, index - (index / width) * width)
}

fn get_area(x: usize, y: usize) -> HashSet<(usize, usize)> {
    (x.saturating_sub(1)..x + 2)
        .flat_map(|x| (y.saturating_sub(1)..y + 2).map(|y| (x, y)).collect_vec())
        .collect::<HashSet<_>>()
}

#[test]
fn test_to_xy() {
    let expected: Vec<(usize, usize)> = vec![
        (0, 0),
        (0, 1),
        (0, 2),
        (1, 0),
        (1, 1),
        (1, 2),
        (2, 0),
        (2, 1),
        (2, 2),
    ];

    let test = (0..9).map(|x| to_xy(x, 3)).collect_vec();

    assert_eq!(&test, &expected);
}

#[test]
fn test_get_area() {
    let expected: HashSet<(usize, usize)> = [
        (0, 0),
        (0, 1),
        (0, 2),
        (1, 0),
        (1, 1),
        (1, 2),
        (2, 0),
        (2, 1),
        (2, 2),
    ]
    .iter()
    .cloned()
    .collect::<HashSet<_>>();

    let test = get_area(1, 1);
    assert_eq!(test, expected);
}

#[test]
fn test_engine() {
    let input_str = r#"467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..
"#;

    let engine = Engine::from_str(input_str).unwrap();
    assert_eq!((engine.width, engine.height), (10, 10));
    assert_eq!(engine.symbols.len(), 6);
    assert_eq!(
        engine.symbols,
        vec![
            Symbol {
                x: 1,
                y: 3,
                is_gear: true,
                area: get_area(1, 3),
            },
            Symbol {
                x: 3,
                y: 6,
                is_gear: false,
                area: get_area(3, 6),
            },
            Symbol {
                x: 4,
                y: 3,
                is_gear: true,
                area: get_area(4, 3),
            },
            Symbol {
                x: 5,
                y: 5,
                is_gear: false,
                area: get_area(5, 5),
            },
            Symbol {
                x: 8,
                y: 3,
                is_gear: false,
                area: get_area(8, 3),
            },
            Symbol {
                x: 8,
                y: 5,
                is_gear: true,
                area: get_area(8, 5),
            }
        ]
    );
    assert_eq!(engine.parts.len(), 10)
}

#[test]
fn test_result1() {
    let input_str = r#"467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..
"#;

    let engine = Engine::from_str(input_str).unwrap();
    let solution = result1(&engine).unwrap();
    assert_eq!(solution, "4361");
}

#[test]
fn test_result2() {
    let input_str = r#"467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..
"#;

    let engine = Engine::from_str(input_str).unwrap();
    let solution = result2(&engine).unwrap();
    assert_eq!(solution, "467835");
}

#[test]
fn test_1() {
    let input_str = r#"******
*....*
*.33.*
*....*
******
"#;

    let engine = Engine::from_str(input_str).unwrap();
    let solution = result1(&engine).unwrap();
    assert_eq!(solution, "0");
}

#[test]
fn test_2() {
    let input_str = r#"....
.11.
*...
.13.
....
...*
.15.
....
"#;

    let engine = Engine::from_str(input_str).unwrap();
    let solution = result1(&engine).unwrap();
    assert_eq!(solution, "39");
}

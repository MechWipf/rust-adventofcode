use std::fmt;

pub type Word = i64;

#[derive(Clone, Copy)]
struct OpMode(Word, Word, Word, Word);

#[derive(Clone, Copy, PartialEq)]
pub enum IntCodeStatus {
    Stopped,
    Running,
    Faulted,
    WaitForInput,
    WaitForOutput,
}

impl fmt::Display for IntCodeStatus {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            IntCodeStatus::Stopped => write!(f, "Stopped"),
            IntCodeStatus::Running => write!(f, "Running"),
            IntCodeStatus::Faulted => write!(f, "Faulted"),
            IntCodeStatus::WaitForInput => write!(f, "WaitForInput"),
            IntCodeStatus::WaitForOutput => write!(f, "WaitForOutput"),
        }
    }
}

pub struct IntCode {
    memory: Vec<Word>,
    pointer: usize,
    current_status: IntCodeStatus,
    current_op: Option<OpMode>,
    stack: Vec<Word>,
    relative_base: Word,
}

impl IntCode {
    pub fn new(initial_memory: &[Word]) -> IntCode {
        let mut memory = vec![0; 1024 * 1024 * 4];

        for (index, x) in initial_memory.iter().enumerate() {
            memory[index] = *x;
        }

        IntCode {
            memory,
            pointer: 0,
            current_status: IntCodeStatus::Stopped,
            current_op: None,
            stack: Vec::new(),
            relative_base: 0,
        }
    }

    pub fn set_input(&mut self, inp: Word) {
        if self.current_status == IntCodeStatus::WaitForInput {
            self.current_status = IntCodeStatus::Stopped;
            self.stack.push(inp);
        };
    }

    pub fn get_output(&mut self) -> Option<Word> {
        if self.current_status == IntCodeStatus::WaitForOutput && !self.stack.is_empty() {
            self.current_status = IntCodeStatus::Stopped;
            Some(self.stack.remove(0))
        } else {
            None
        }
    }

    pub async fn execute(&mut self) -> IntCodeStatus {
        self.run();
        self.get_status()
    }

    pub fn run(&mut self) {
        if self.current_status != IntCodeStatus::Stopped {
            panic!(
                "Can only start intcode vm from status stopped. Current Status: {}",
                self.current_status
            );
        }

        self.current_status = IntCodeStatus::Running;

        loop {
            let current_op = if let Some(cop) = self.current_op {
                cop
            } else {
                let cop = self.get_next_op();
                self.current_op = Some(cop);
                cop
            };

            let OpMode(op, mode1, mode2, mode3) = current_op;

            match op {
                // add
                1 => {
                    let source1 = self.get_with_mode(mode1);
                    let source2 = self.get_with_mode(mode2);
                    self.set_with_mode(source1 + source2, mode3);
                }
                // multiply
                2 => {
                    let source1 = self.get_with_mode(mode1);
                    let source2 = self.get_with_mode(mode2);
                    self.set_with_mode(source1 * source2, mode3);
                }
                // input
                3 => {
                    if !self.stack.is_empty() {
                        let input = self.stack.remove(0);
                        self.set_with_mode(input, mode1);
                    } else {
                        self.current_status = IntCodeStatus::WaitForInput;
                        break;
                    }
                }
                // output
                4 => {
                    let source = self.get_with_mode(mode1);
                    self.stack.push(source);
                    self.current_status = IntCodeStatus::WaitForOutput;
                    self.current_op = None;
                    break;
                }
                // jmp if true
                5 => {
                    let source1 = self.get_with_mode(mode1);
                    let source2 = self.get_with_mode(mode2);

                    if source1 != 0 {
                        self.pointer = source2 as usize
                    };
                }
                // jmp if false
                6 => {
                    let source1 = self.get_with_mode(mode1);
                    let source2 = self.get_with_mode(mode2);

                    if source1 == 0 {
                        self.pointer = source2 as usize
                    };
                }
                // less then
                7 => {
                    let source1 = self.get_with_mode(mode1);
                    let source2 = self.get_with_mode(mode2);
                    let value = if source1 < source2 { 1 } else { 0 };
                    self.set_with_mode(value, mode3);
                }
                // equals
                8 => {
                    let source1 = self.get_with_mode(mode1);
                    let source2 = self.get_with_mode(mode2);
                    let value = if source1 == source2 { 1 } else { 0 };
                    self.set_with_mode(value, mode3);
                }
                // set relative base
                9 => {
                    let source = self.get_with_mode(mode1);
                    self.relative_base += source;
                }
                // stop
                99 => {
                    break;
                }
                // whatever
                _ => {
                    self.current_status = IntCodeStatus::Faulted;
                    break;
                }
            }

            self.current_op = None;
        }

        if self.current_status == IntCodeStatus::Running {
            self.current_status = IntCodeStatus::Stopped;
        }
    }

    pub fn get_status(&self) -> IntCodeStatus {
        self.current_status
    }

    fn get_next_op(&mut self) -> OpMode {
        let op_and_mode = self.memory[self.pointer];
        self.pointer += 1;

        let opcode = op_and_mode % 100;
        let mode1 = (op_and_mode / 100) % 10;
        let mode2 = (op_and_mode / 1000) % 10;
        let mode3 = (op_and_mode / 10000) % 10;
        OpMode(opcode, mode1, mode2, mode3)
    }

    fn get_with_mode(&mut self, mode: Word) -> Word {
        let p: Word = self.memory[self.pointer];
        self.pointer += 1;

        if mode == 1 {
            p
        } else if mode == 2 {
            self.memory[(self.relative_base + p) as usize]
        } else {
            self.memory[p as usize]
        }
    }

    fn set_with_mode(&mut self, value: Word, mode: Word) {
        let target = self.memory[self.pointer];
        self.pointer += 1;

        let target_pointer = if mode == 2 {
            self.relative_base + target
        } else {
            target
        };

        self.memory[target_pointer as usize] = value;
    }
}

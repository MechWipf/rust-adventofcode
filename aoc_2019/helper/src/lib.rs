use async_std::{fs::File, prelude::*};
use std::collections::HashMap;
use std::io::prelude::*;
use std::path::Path;

pub mod intcode;

pub fn read_input_file(file_path: &Path) -> String {
    let display = file_path.display();

    let mut file = match std::fs::File::open(file_path) {
        Err(why) => panic!("could not open {}: {}", display, why),
        Ok(file) => file,
    };

    let mut s = String::new();
    match file.read_to_string(&mut s) {
        Err(why) => panic!("could not read {}: {}", display, why),
        Ok(_) => s,
    }
}

pub fn str_group_by_char(input: &str) -> HashMap<char, i32> {
    let mut collect: HashMap<char, i32> = HashMap::new();

    for ch in input.chars() {
        collect.entry(ch).or_insert(0);
        if let Some(val) = collect.get_mut(&ch) {
            *val += 1;
        }
    }

    collect
}

pub async fn async_read_input_file(path: &Path) -> Result<String, Box<dyn std::error::Error>> {
    let mut file = File::open(path).await?;
    let mut contents = String::new();
    file.read_to_string(&mut contents).await?;

    Ok(contents)
}

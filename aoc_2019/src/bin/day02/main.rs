use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

fn main() {
    let path = Path::new("input.txt");
    let input_string = read_input_file(path);
    let mut memory: Vec<i32> = Vec::new();

    for line in input_string.split(',') {
        memory.push(match line.parse() {
            Err(why) => panic!("could not parse {}: {}", line, why),
            Ok(n) => n,
        });
    }

    let mut result = 0;
    'outer: for x in 0..99 {
        for y in 0..99 {
            if run_intcode(x, y, &memory) == 19690720 {
                result = 100 * x + y;
                break 'outer;
            }
        }
    }

    println!("Solution: {}", result);
}

fn read_input_file(file_path: &Path) -> String {
    let display = file_path.display();

    let mut file = match File::open(file_path) {
        Err(why) => panic!("could not open {}: {}", display, why),
        Ok(file) => file,
    };

    let mut s = String::new();
    match file.read_to_string(&mut s) {
        Err(why) => panic!("could not read {}: {}", display, why),
        Ok(_) => s,
    }
}

fn run_intcode(noun: i32, verb: i32, memory_seed: &[i32]) -> i32 {
    let mut memory = vec![0; memory_seed.len()];
    memory.copy_from_slice(&memory_seed[0..]);

    memory[1] = noun;
    memory[2] = verb;

    let mut pointer = 0;
    loop {
        let operation = memory[pointer];
        pointer += 1;

        match operation {
            1 => {
                let source1 = memory[pointer] as usize;
                pointer += 1;
                let source2 = memory[pointer] as usize;
                pointer += 1;
                let target = memory[pointer] as usize;
                pointer += 1;

                memory[target] = memory[source1] + memory[source2];
            }
            2 => {
                let source1 = memory[pointer] as usize;
                pointer += 1;
                let source2 = memory[pointer] as usize;
                pointer += 1;
                let target = memory[pointer] as usize;
                pointer += 1;

                memory[target] = memory[source1] * memory[source2];
            }
            99 => {
                break memory[0];
            }
            _ => {
                break -1;
            }
        }
    }
}

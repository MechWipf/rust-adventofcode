use async_std::{fs::File, prelude::*, task};
use helper::intcode::{IntCode, IntCodeStatus, Word};
use std::path::Path;

fn main() {
    let main_task = task::spawn(async_main());
    task::block_on(main_task);
}

async fn async_main() {
    let path = Path::new("input/day09.txt");
    let mut memory: Vec<Word> = Vec::new();

    if let Err(e) = from_input(&mut memory, path).await {
        panic!("failed to read input: {}", e)
    };

    solution(&memory, 1).await;
    solution(&memory, 2).await;
}

async fn solution(initial_memory: &[Word], input: Word) {
    let mut runner = IntCode::new(initial_memory);

    loop {
        match runner.execute().await {
            IntCodeStatus::Stopped => {
                break;
            }
            IntCodeStatus::WaitForInput => {
                runner.set_input(input);
            }
            IntCodeStatus::WaitForOutput => {
                println!("Output: {}", runner.get_output().unwrap_or(-1));
            }
            IntCodeStatus::Faulted => panic!("IntCode VM is Faulted"),
            _ => panic!("Unknown state"),
        };
    }
}

async fn from_input(memory: &mut Vec<Word>, path: &Path) -> Result<(), Box<dyn std::error::Error>> {
    let mut file = File::open(path).await?;
    let mut contents = String::new();
    file.read_to_string(&mut contents).await?;
    let contents = contents.replace('\n', "");

    for line in contents.split(',') {
        let parsed = line.parse::<Word>()?;
        memory.push(parsed);
    }

    Ok(())
}

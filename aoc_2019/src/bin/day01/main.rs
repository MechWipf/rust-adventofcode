use std::path::Path;

fn main() {
    let path = Path::new("input.txt");
    let input_string = helper::read_input_file(path);

    part1(&input_string);
    part2(&input_string);
}

fn part1(input: &str) {
    let lines = input.lines();
    let mut count = 0;

    for line in lines {
        let num = match line.parse::<i32>() {
            Err(why) => panic!("could not parse number {}: {}", line, why),
            Ok(n) => n,
        };

        count += num / 3 - 2;
    }

    println!("Solution Part 1: {}", count);
}

fn part2(input: &str) {
    let lines = input.lines();
    let mut count = 0;

    for line in lines {
        let num = match line.parse::<i32>() {
            Err(why) => panic!("could not parse number {}: {}", line, why),
            Ok(n) => n,
        };

        count += calc_fuel_cost(num);
    }

    println!("Solution Part 2: {}", count);
}

fn calc_fuel_cost(mass: i32) -> i32 {
    if mass < 3 {
        0
    } else {
        let fuel = i32::max(0, mass / 3 - 2);
        fuel + calc_fuel_cost(fuel)
    }
}

use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

struct OpMode(i32, i32, i32, i32);

fn main() {
    let path = Path::new("input.txt");
    let input_string = read_input_file(path);
    let mut memory: Vec<i32> = Vec::new();

    for line in input_string.split(',') {
        memory.push(match line.parse() {
            Err(why) => panic!("could not parse {}: {}", line, why),
            Ok(n) => n,
        });
    }

    let result = run_intcode(5, &memory);

    println!("Solution: {}", result);
}

fn read_input_file(file_path: &Path) -> String {
    let display = file_path.display();

    let mut file = match File::open(file_path) {
        Err(why) => panic!("could not open {}: {}", display, why),
        Ok(file) => file,
    };

    let mut s = String::new();
    match file.read_to_string(&mut s) {
        Err(why) => panic!("could not read {}: {}", display, why),
        Ok(_) => s,
    }
}

fn run_intcode(input: i32, memory_seed: &[i32]) -> i32 {
    let pointer = &mut 0;
    let mut memory = vec![0; memory_seed.len()];

    memory.copy_from_slice(&memory_seed[0..]);

    loop {
        let OpMode(op, mode1, mode2, _mode3) = get_next_op(pointer, &mut memory);

        match op {
            // add
            1 => {
                let source1 = get_with_mode(pointer, &mut memory, mode1);
                let source2 = get_with_mode(pointer, &mut memory, mode2);
                let target = get_with_mode(pointer, &mut memory, 1);

                memory[target as usize] = source1 + source2;
            }
            // multiply
            2 => {
                let source1 = get_with_mode(pointer, &mut memory, mode1);
                let source2 = get_with_mode(pointer, &mut memory, mode2);
                let target = get_with_mode(pointer, &mut memory, 1);

                memory[target as usize] = source1 * source2;
            }
            // input
            3 => {
                let target: i32 = get_with_mode(pointer, &mut memory, 1);

                memory[target as usize] = input;
            }
            // output
            4 => {
                let source = get_with_mode(pointer, &mut memory, mode1);
                println!("output: {}", source);
            }
            // jmp if true
            5 => {
                let source1 = get_with_mode(pointer, &mut memory, mode1);
                let source2 = get_with_mode(pointer, &mut memory, mode2);

                if source1 != 0 {
                    *pointer = source2 as usize
                };
            }
            // jmp if false
            6 => {
                let source1 = get_with_mode(pointer, &mut memory, mode1);
                let source2 = get_with_mode(pointer, &mut memory, mode2);

                if source1 == 0 {
                    *pointer = source2 as usize
                };
            }
            // less then
            7 => {
                let source1 = get_with_mode(pointer, &mut memory, mode1);
                let source2 = get_with_mode(pointer, &mut memory, mode2);
                let target = get_with_mode(pointer, &mut memory, 1);

                memory[target as usize] = if source1 < source2 { 1 } else { 0 };
            }
            // equals
            8 => {
                let source1 = get_with_mode(pointer, &mut memory, mode1);
                let source2 = get_with_mode(pointer, &mut memory, mode2);
                let target = get_with_mode(pointer, &mut memory, 1);

                memory[target as usize] = if source1 == source2 { 1 } else { 0 };
            }
            99 => {
                break memory[0];
            }
            _ => {
                break -1;
            }
        }
    }
}

fn get_next_op(pointer: &mut usize, memory: &mut Vec<i32>) -> OpMode {
    let op_and_mode = (*memory)[*pointer];
    *pointer += 1;

    let opcode = op_and_mode % 100;
    let mode1 = (op_and_mode / 100) % 10;
    let mode2 = (op_and_mode / 1000) % 10;
    let mode3 = (op_and_mode / 10000) % 10;

    OpMode(opcode, mode1, mode2, mode3)
}

fn get_with_mode(pointer: &mut usize, memory: &mut Vec<i32>, mode: i32) -> i32 {
    let p: i32 = (*memory)[*pointer];
    *pointer += 1;

    if mode == 1 {
        p
    } else {
        (*memory)[p as usize]
    }
}

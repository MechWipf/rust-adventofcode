use std::path::Path;

mod orbital {
    #[derive(Debug)]
    pub struct OrbitalObject {
        pub name: String,
        pub parent: Option<String>,
    }
}

fn main() {
    let input_file = Path::new("input/day06.txt");
    let input_string = helper::read_input_file(input_file);

    let mut objects: Vec<orbital::OrbitalObject> = Vec::new();

    let com = orbital::OrbitalObject {
        name: String::from("COM"),
        parent: None,
    };
    objects.push(com);

    for line in input_string.lines() {
        let v: Vec<&str> = line.split(')').collect();

        let x = orbital::OrbitalObject {
            name: String::from(v[1]),
            parent: Some(String::from(v[0])),
        };
        objects.push(x);
    }

    // println!("{:#?}", objects);

    let mut count = 0;
    for x in &objects {
        count += count_parents(x, &objects, 0)
    }
    println!("Solution: {}", count);

    let path_to_you = if let Some(x) = objects.iter().find(|o| o.name == "YOU") {
        get_parents(x, &objects)
    } else {
        panic!("Did not find YOU in orbit.");
    };

    let path_to_santa = if let Some(x) = objects.iter().find(|o| o.name == "SAN") {
        get_parents(x, &objects)
    } else {
        panic!("Did not find SAN in orbit.");
    };

    count = 0;
    let mut first: &str = "";

    for x in &path_to_you {
        if path_to_santa.contains(x) {
            first = x;
            break;
        };
        count += 1;
    }

    for x in &path_to_santa {
        if first == x {
            break;
        };
        count += 1;
    }

    println!("{}, {}", first, count);
}

fn count_parents(
    object: &orbital::OrbitalObject,
    map: &[orbital::OrbitalObject],
    count: i32,
) -> i32 {
    if let Some(parent_name) = &object.parent {
        if let Some(x) = map.iter().find(|o| *o.name == *parent_name) {
            return count_parents(x, map, count + 1);
        }
    };

    count
}

fn get_parents(object: &orbital::OrbitalObject, map: &[orbital::OrbitalObject]) -> Vec<String> {
    let mut parents: Vec<String> = Vec::new();
    let mut cur_object = object;

    loop {
        let parent: Option<&orbital::OrbitalObject> = if let Some(parent_name) = &cur_object.parent
        {
            map.iter().find(|o| *o.name == *parent_name)
        } else {
            None
        };

        if parent.is_none() {
            break;
        };

        parents.push(String::from(&parent.unwrap().name));
        cur_object = parent.unwrap();
    }

    parents
}

use std::path::Path;

fn main() {
    let width = 25;
    let height = 6;

    let input_string = get_prepared_input();
    let data = convert_to_layers(&input_string, width, height);

    for elem in data.iter().enumerate() {
        let (index, d) = elem;

        println!(
            "Current Layer {}, Zero {}, One {}, Two {}",
            index,
            d.iter().filter(|&x| *x == 0).count(),
            d.iter().filter(|&x| *x == 1).count(),
            d.iter().filter(|&x| *x == 2).count(),
        )
    }

    solution_part1(&data);
    solution_part2(&data, width as usize, height as usize);
}

fn solution_part1(data: &[Vec<u32>]) {
    let fewest_zeroes = get_fewest_zeroes(data);
    let ones = data[fewest_zeroes].iter().filter(|&x| *x == 1).count();
    let twos = data[fewest_zeroes].iter().filter(|&x| *x == 2).count();

    println!(
        "Layer with lowest 0 count: {}; Count of 1 {}; Count of 2 {}; Multiplicated: {}",
        fewest_zeroes,
        ones,
        twos,
        ones * twos
    );
}

fn solution_part2(data: &[Vec<u32>], width: usize, height: usize) {
    for y in 0..height {
        for x in 0..width {
            let index = x + (width * y);
            let pixel = get_visible_pixel(data, 0, index);

            print!(
                "{}",
                match pixel {
                    0 => " ",
                    1 => "#",
                    _ => ".",
                }
            );
        }
        println!();
    }
}

fn get_visible_pixel(data: &[Vec<u32>], layer: usize, index: usize) -> u32 {
    let pixel_layer = data.get(layer);

    if pixel_layer.is_none() {
        return 2;
    }

    let pixel = pixel_layer.unwrap()[index];

    if pixel == 2 {
        get_visible_pixel(data, layer + 1, index)
    } else {
        pixel
    }
}

fn get_prepared_input() -> String {
    let path = Path::new("input/day08.txt");
    helper::read_input_file(path).replace('\n', "")
}

fn convert_to_layers(input: &str, width: i32, height: i32) -> Vec<Vec<u32>> {
    let mut outer_vector: Vec<Vec<u32>> = Vec::new();
    let mut index = 0;

    outer_vector.push(Vec::new());
    for chr in input.chars() {
        if index == width * height {
            index = 0;
            outer_vector.push(Vec::new());
        }

        let current_vector = &mut outer_vector.last_mut().unwrap();

        let num = chr.to_digit(10).unwrap();
        current_vector.push(num);

        index += 1;
    }

    outer_vector
}

fn get_fewest_zeroes(layers: &[Vec<u32>]) -> usize {
    let mut min = 1000;
    let mut min_index = 0;

    for kvp in layers.iter().enumerate() {
        let (index, layer) = kvp;
        let count_of_zero = layer.iter().filter(|&x| *x == 0).count();

        if count_of_zero < min {
            min = count_of_zero;
            min_index = index;
        }
    }

    min_index
}

use std::cmp::PartialEq;
use std::f32;
use std::ops::{Add, Sub};

#[derive(Debug)]
pub struct Point {
    pub x: f32,
    pub y: f32,
}

impl Point {
    fn _dist(self, other: Point) -> f32 {
        let so = self - other;
        f32::sqrt(so.x * so.x + so.y * so.y)
    }
}

impl Add for Point {
    type Output = Point;

    fn add(self, other: Point) -> Point {
        Point {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl Sub for Point {
    type Output = Point;

    fn sub(self, other: Point) -> Point {
        Point {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl PartialEq for Point {
    fn eq(&self, other: &Point) -> bool {
        self.x == other.x && self.y == other.y
    }
}

pub mod line {
    use super::Point;

    pub fn slope(from: &Point, to: &Point) -> f32 {
        f32::atan2(from.x - to.x, from.y - to.y)
    }
}

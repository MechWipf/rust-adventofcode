mod vector;

use async_std::task;
use helper::async_read_input_file;
use itertools::Itertools;
use pancurses::{endwin, initscr, Input};
use std::{char, cmp, fmt, path::Path, sync::mpsc};
use vector::{line, Point};

static SLEEP_DURATION: std::time::Duration = std::time::Duration::from_millis(15);

#[derive(Clone, Copy, Eq)]
struct Field(Option<i32>);

impl fmt::Display for Field {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let arg: String = match &self.0 {
            None => String::from("."),
            Some(x) => x.to_string(),
        };

        write!(f, "{}", arg)
    }
}

impl std::cmp::Ord for Field {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.0.cmp(&other.0)
    }
}

impl std::cmp::PartialOrd for Field {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl std::cmp::PartialEq for Field {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

fn main() {
    let (tx, rx) = mpsc::channel();
    let mut handles = vec![];

    handles.push(task::spawn(async_draw(rx)));
    handles.push(task::spawn(async_solution(tx)));

    for handle in handles {
        task::block_on(handle);
    }
}

async fn async_solution(tx: mpsc::Sender<String>) {
    let path = Path::new("input/day10.txt");
    let input_string = async_read_input_file(path).await.unwrap();

    {
        let msg = format!("Processing Input. Chars: {}\n", input_string.len());
        tx.send(msg).ok();
    }

    let mut field: Vec<Vec<Field>> = vec![];
    create_field_from(&mut field, &input_string).unwrap();
    run_raytrace(&mut field);

    for row in &field {
        tx.send(format!(
            "{}\n",
            row.iter()
                .map(|&x| match x.0 {
                    Some(num) => char::from_digit(cmp::min(9, num as u32), 10).unwrap_or('-'),
                    None => '.',
                })
                .collect::<String>()
        ))
        .ok();
    }

    let max_val = field.iter().flatten().max();

    {
        let msg = format!("Solution for Part one is {}\n", max_val.unwrap());
        tx.send(msg).ok();
    }

    let monitoring_station = get_max_point(&field).unwrap();
    let _list_of_shooted_astroids = shoot_astroids(&field, &monitoring_station);
}

fn shoot_astroids(field: &[Vec<Field>], station: &Point) -> Vec<Point> {
    let shoot = vec![];
    let astroids = get_all_astroids(field);
    let astroids = astroids
        .iter()
        .sorted_by(|a, b| {
            ((line::slope(station, a) * 1000.0) as i32)
                .cmp(&((line::slope(station, b) * 1000.0) as i32))
        })
        .group_by(|x| (line::slope(station, x) * 1000.0).round() as i32);

    for (key, grp) in &astroids {
        println!("{} -> {:?}", key, grp.collect::<Vec<_>>());
    }

    shoot
}

fn get_max_point(field: &[Vec<Field>]) -> Option<Point> {
    let max_val = field.iter().flatten().max().unwrap();

    let mut actual = None;
    for (y, row) in field.iter().enumerate() {
        for (x, cell) in row.iter().enumerate() {
            if max_val == cell {
                actual = Some(Point {
                    x: x as f32,
                    y: y as f32,
                });

                break;
            }
        }
    }

    actual
}

fn create_field_from(
    field: &mut Vec<Vec<Field>>,
    from_input: &str,
) -> Result<(), Box<dyn std::error::Error>> {
    for line in from_input.lines() {
        field.push(vec![]);
        let row = field.last_mut().unwrap();

        for c in line.chars() {
            match c {
                '.' => row.push(Field(None)),
                '#' => row.push(Field(Some(0))),
                _ => (),
            };
        }
    }

    Ok(())
}

fn run_raytrace(field: &mut [Vec<Field>]) {
    let astroids = get_all_astroids(field);

    for astroid1 in &astroids {
        let all_slopes = astroids
            .iter()
            .filter(|&x| x != astroid1)
            .map(|x| (line::slope(astroid1, x) * 1000.0).round() as i32)
            .unique()
            .count() as i32;

        field[astroid1.y as usize][astroid1.x as usize] = Field(Some(all_slopes));
    }
}

fn get_all_astroids(field: &[Vec<Field>]) -> Vec<Point> {
    let mut astroids = vec![];

    for (y, row) in field.iter().enumerate() {
        for (x, cell) in row.iter().enumerate() {
            if cell.0.is_some() {
                astroids.push(Point {
                    x: x as f32,
                    y: y as f32,
                })
            }
        }
    }

    astroids
}

async fn async_draw(rx: mpsc::Receiver<String>) {
    let window = initscr();
    window.nodelay(true);
    pancurses::noecho();

    'outer: loop {
        if let Some(Input::Character('q')) = window.getch() {
            break 'outer;
        };

        'channel: loop {
            if let Ok(message) = rx.try_recv() {
                window.printw(message);
            } else {
                break 'channel;
            }
        }

        std::thread::sleep(SLEEP_DURATION);
    }

    endwin();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_field_from_input() {
        let p = async {
            let path = Path::new("input/day10.example3.txt");
            let input_string = async_read_input_file(path).await.unwrap();
            let mut field: Vec<Vec<Field>> = vec![];
            create_field_from(&mut field, &input_string).unwrap();

            let expected = ".#..#\n.....\n#####\n....#\n...##\n";
            let actual = field
                .iter()
                .map(|r| {
                    r.iter()
                        .map(|&c| match c.0 {
                            Some(_) => "#",
                            None => ".",
                        })
                        .collect::<String>()
                        + "\n"
                })
                .collect::<String>();

            assert_eq!(actual, expected);
        };

        task::block_on(p);
    }

    #[test]
    fn sample3_is_correct() {
        let p = async {
            let path = Path::new("input/day10.example3.txt");
            let input_string = async_read_input_file(path).await.unwrap();
            let mut field: Vec<Vec<Field>> = vec![];
            create_field_from(&mut field, &input_string).unwrap();
            run_raytrace(&mut field);

            let expected = ".7..7\n.....\n67775\n....7\n...87\n";
            let actual = field
                .iter()
                .map(|r| r.iter().map(|&c| c.to_string()).collect::<String>() + "\n")
                .collect::<String>();

            assert_eq!(actual, expected);
        };

        task::block_on(p);
    }

    #[test]
    fn sample1_is_correct() {
        let p = async {
            let actual = get_max_point("input/day10.example1.txt").await;
            let expected = Some(Point { x: 11.0, y: 13.0 });

            assert_eq!(actual, expected);
        };

        task::block_on(p);
    }

    #[test]
    fn sample2_is_correct() {
        let p = async {
            let actual = get_max_point("input/day10.example2.txt").await;
            let expected = Some(Point { x: 5.0, y: 8.0 });

            assert_eq!(actual, expected);
        };

        task::block_on(p);
    }

    async fn get_max_point(path_str: &str) -> Option<Point> {
        let path = Path::new(path_str);
        let input_string = async_read_input_file(path).await.unwrap();
        let mut field: Vec<Vec<Field>> = vec![];
        create_field_from(&mut field, &input_string).unwrap();
        run_raytrace(&mut field);

        let max_val = field.iter().flatten().max().unwrap();

        let mut actual = None;
        for (y, row) in field.iter().enumerate() {
            for (x, cell) in row.iter().enumerate() {
                if max_val == cell {
                    actual = Some(Point {
                        x: x as f32,
                        y: y as f32,
                    });

                    break;
                }
            }
        }

        actual
    }
}

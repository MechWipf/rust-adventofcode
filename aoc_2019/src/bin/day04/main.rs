use helper::str_group_by_char;

fn main() {
    let mut count1 = 0;
    let mut count2 = 0;

    for n in 245182..790572 {
        let passcode = n.to_string();
        if valid_passcode(&passcode, false) {
            count1 += 1;
        }

        if valid_passcode(&passcode, true) {
            count2 += 1;
        }
    }

    print!("Solution 1: {}\n Solution 2: {}", count1, count2);
}

fn is_sorted(passcode: &str) -> bool {
    for index in 1..passcode.len() {
        if passcode.get(index - 1..index) > passcode.get(index..index + 1) {
            return false;
        }
    }

    true
}

fn is_double(passcode: &str) -> bool {
    return str_group_by_char(passcode).values().any(|&a| a > 1);
}

fn is_double_specific(passcode: &str) -> bool {
    return str_group_by_char(passcode).values().any(|&a| a == 2);
}

fn valid_passcode(passcode: &str, part2: bool) -> bool {
    if part2 {
        is_sorted(passcode) && is_double_specific(passcode)
    } else {
        is_sorted(passcode) && is_double(passcode)
    }
}

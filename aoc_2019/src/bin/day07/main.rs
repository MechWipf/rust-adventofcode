use helper::intcode::IntCode;
use helper::intcode::IntCodeStatus;
use helper::read_input_file;
use helper::str_group_by_char;
use permutate::Permutator;
use std::path::Path;

fn main() {
    let path = Path::new("input/day07.txt");
    let input_string = read_input_file(path);
    let mut memory: Vec<i64> = Vec::new();

    for line in input_string.split(',') {
        memory.push(match line.parse() {
            Err(why) => panic!("could not parse {}: {}", line, why),
            Ok(n) => n,
        });
    }

    let mut max = 0;
    for perm in get_permutations() {
        let mut value = 0;
        // print!("{:?}", perm);

        for p in perm.get(0..5).unwrap() {
            let mut intcode = IntCode::new(&memory);
            intcode.run();
            intcode.set_input((*p).into());
            intcode.run();
            intcode.set_input(value);
            intcode.run();
            value = intcode.get_output().unwrap();
            intcode.run();
        }

        if value > max {
            max = value;
        }

        // println!(" {}", value);
    }

    println!("Solution Part 1: {}", max);

    let mut runners: Vec<Box<IntCode>> = Vec::new();
    let mut max = 0;
    for perm in get_permutations() {
        let mut value = 0;

        for i in 0..5 {
            runners.push(Box::new(IntCode::new(&memory)));

            let intcode = &mut runners[i];
            intcode.run();
            intcode.set_input((perm[i] + 5).into());
            intcode.run();
        }

        'boop: loop {
            let mut last_status = IntCodeStatus::Running;
            let mut dbg: Vec<i64> = Vec::new();

            for intcode in runners.get_mut(0..5).unwrap() {
                dbg.push(value);
                intcode.set_input(value);
                intcode.run();
                value = intcode.get_output().unwrap();
                intcode.run();
                last_status = intcode.get_status();
            }

            if last_status == IntCodeStatus::Stopped {
                break 'boop;
            }
        }

        if value > max {
            max = value;
        }

        runners.clear();
    }

    println!("Solution Part 2: {}", max);
}

fn get_permutations() -> Vec<Vec<i32>> {
    let mut ret: Vec<Vec<i32>> = Vec::new();
    let list: &[&i32] = &[&0, &1, &2, &3, &4];
    let list = [list];
    let mut permutator = Permutator::new(&list[..]);

    loop {
        if let Some(permutation) = permutator.next() {
            let mut vec: Vec<i32> = Vec::new();
            let mut num: String = "".to_owned();

            for element in permutation {
                vec.push(*element);
                num.push_str(&element.to_string());
            }

            if !str_group_by_char(&num).values().any(|&x| x > 1) {
                ret.push(vec);
            }
        } else {
            return ret;
        }
    }
}
